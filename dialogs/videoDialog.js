const {
  ComponentDialog,
  TextPrompt,
  WaterfallDialog,
} = require("botbuilder-dialogs");

const {
  getUserProfile,
  updateReplyFlag,
  postVideoSubmission,
  getStudentVideo,
} = require("../firebase/firebaseHelper");
// const { checkLuisConfirm } = require("../luis/luisHelper");
const { getChannel } = require("../utils/localFileHelper");
const { downloadVideo } = require("../transcode/transcoder");
const ROLE = "Student";

class VideoDialog extends ComponentDialog {
  constructor() {
    super("VideoDialog");

    // Define the main dialog and its related components.
    this.addDialog(new TextPrompt("TextPrompt", this.linkPromptValidator));
    this.addDialog(new TextPrompt("ConfirmTextPrompt", this.confirmValidator));

    this.addDialog(
      new WaterfallDialog("MainWaterFallDialog", [
        this.askVideoUrlStep.bind(this),
        this.confirmVideoStep.bind(this),
        this.confirmUploadStep.bind(this),
      ])
    );

    this.initialDialogId = "MainWaterFallDialog";
  }

  /**
   * Prompts the user for the video submission.
   * @param {WaterfallStepContext} stepContext
   */
  async askVideoUrlStep(stepContext) {
    // Read id from stepContext and save it in stepContext.values.
    const id = stepContext.options.id;
    stepContext.values.id = id;
    const { name } = await getUserProfile(id, ROLE);
    stepContext.values.name = name;

    // Retrieve the video url from the current activity.
    var videoUrl;
    try {
      videoUrl = stepContext.context.activity.text;
      stepContext.values.videoUrl = videoUrl;
    } catch (error) {
      return await stepContext.endDialog();
    }

    if (
      videoUrl &&
      (videoUrl.startsWith("https://drive.google.com/") ||
        videoUrl.startsWith("https://spark.adobe.com/video/") ||
        videoUrl.startsWith("https://1drv.ms/"))
    )
      return await stepContext.next();
    else {
      var promptOptions = {
        prompt: "Could you send me the link to your video again?",
        retryPrompt:
          "I couldn't download your video from the link you provided 😵 Can you check it and send it to me again? A valid video link should start with `https://drive.google.com/`, `https://spark.adobe.com/video/` or `https://1drv.ms/`.",
      };

      return await stepContext.prompt("TextPrompt", promptOptions);
    }
  }

  /**
   * Confirm with the student about the video submission.
   * @param {WaterfallStepContext} stepContext
   */
  async confirmVideoStep(stepContext) {
    if (stepContext.result) stepContext.values.videoUrl = stepContext.result;

    const id = stepContext.values.id;
    const videoUrl = stepContext.values.videoUrl;
    const userId = stepContext.context.activity.from.id;
    const channel = getChannel(stepContext.context);

    await updateReplyFlag(id, ROLE, false);
    downloadVideo(videoUrl, id, channel, userId);

    // Ask for confirmation after the video transcoding.
    // Since the downloading can be a long-running process, we should only promp the user
    // with an empty confirm prompt, and send the confirmation message after the video
    // has been processed.
    return await stepContext.prompt("ConfirmTextPrompt", {
      retryPrompt:
        "Is this image from the video you wanted to send me? Confirm with yes/no.",
    });
  }

  /**
   * Final step for confirm the uploaded video.
   * @param {WaterfallStepContext} stepContext
   */
  async confirmUploadStep(stepContext) {
    const id = stepContext.values.id;
    const name = stepContext.values.name;
    const videoExistence = await getStudentVideo(id);

    const confirm = stepContext.result.toUpperCase();

    if (confirm === "NO" || !videoExistence)
      return await stepContext.replaceDialog(this.initialDialogId, { id: id });

    const videoUrl = stepContext.values.videoUrl;
    await postVideoSubmission(id, videoUrl);
    await stepContext.context.sendActivity(
      `Thanks for your video ${name}! I’m just waiting on everyone else to send me something and then I’ll let you know what’s next.`
    );
    return await stepContext.endDialog();
  }

  /**
   * Video Link Prompt Validator to check whether the link provided is valid.
   * @param {PromptValidatorContext} promptContext
   */
  async linkPromptValidator(promptContext) {
    const str = promptContext.recognized.value;
    return (
      promptContext.recognized.succeeded &&
      (str.startsWith("https://drive.google.com/") ||
        str.startsWith("https://spark.adobe.com/video/") ||
        str.startsWith("https://1drv.ms/"))
    );
  }

  /**
   * Thumb Prompt Validator to check whether the user replies a confirm message.
   * @param {PromptValidatorContext} promptContext
   */
  async confirmValidator(promptContext) {
    const str = promptContext.recognized.value.toUpperCase();
    return str === "YES" || str === "NO";
  }
}

exports.VideoDialog = VideoDialog;
