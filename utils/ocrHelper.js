const fs = require("fs");
const request = require("request");
const { removeFile } = require("./localFileHelper");

// For production environment, remove the string here.
const SUBSCRIPTION_KEY = process.env.ComputerVisionSubscriptionKey;
const ENDPOINT = process.env.ComputerVisionEndpoint;

if (!SUBSCRIPTION_KEY) {
  throw new Error(
    "Set your environment variables for your subscription key and endpoint."
  );
}

const URI_BASE = ENDPOINT + "vision/v2.1/ocr";

// Request parameters.
const PARAMS = {
  language: "en",
  detectOrientation: "true",
};

const options = {
  uri: URI_BASE,
  qs: PARAMS,
  headers: {
    "Content-Type": "application/octet-stream",
    "Ocp-Apim-Subscription-Key": SUBSCRIPTION_KEY,
  },
};

/**
 * Call the Microsoft Optical Character Recognition method to extract the
 * student id from the uploaded student id card.
 * @param {String} localFileName
 */
async function getOcrResponse(localFileName) {
  const imageData = fs.readFileSync(localFileName);
  options.body = imageData;

  try {
    const { error, response, body } = await asyncRequest(options);

    var str;
    const regions = JSON.parse(body).regions;
    for (i = 0; i < regions.length; i++) {
      const lines = regions[i].lines;
      for (j = 0; j < lines.length; j++) {
        const words = lines[j].words;
        for (k = 0; k < words.length; k++) {
          str = str + " " + words[k].text;
        }
      }
    }

    // Remove the local image file.
    await removeFile(localFileName);

    // Check whether it is valid student id card from Monash.
    if (str && str.includes("MONASH") && str.includes("University"))
      return str.match(/[0-9]{8}/g);
    return null;
  } catch (error) {
    console.error(error);
    return error;
  }
}

/**
 * Wrap the request post method in a Promise.
 * @param {*} options
 */
async function asyncRequest(options) {
  return new Promise((resolve, reject) => {
    request.post(options, (error, response, body) => {
      if (!error) resolve({ error, response, body });
      reject(error);
    });
  });
}

exports.getOcrResponse = getOcrResponse;
