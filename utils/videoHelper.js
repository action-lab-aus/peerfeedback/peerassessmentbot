const { ActivityTypes } = require("botbuilder");
const {
  getUserProfile,
  getUserCount,
  getUserList,
  getStudentIdByIndex,
  getVideoUrl,
  getThumbnailUrl,
  postUserList,
  updateUserList,
} = require("../firebase/firebaseHelper");

/**
 * Retrieve the video owner name assigned to the user for feedback.
 * @param {String} id Current user id number.
 * @param {String} role Role of the current user.
 * @param {Integer} userIndex Current user index.
 */
async function getVideoOwner(id, role, userIndex) {
  const { index } = await getUserProfile(id, role);
  if (role === "Student") {
    // Calculate the first index of the video to be assigned.
    const userCount = await getUserCount();
    const i = ((index + userIndex) % userCount) + 1;
    return await getStudentIdByIndex(i);
  } else {
    let userList = await getUserList();
    let listSize = userList.length;

    if (listSize < 3) {
      // Reset the user list for more ranking.
      await postUserList();
      userList = await getUserList();
      listSize = userList.length;
    }

    const i = Math.floor(Math.random() * listSize);
    let id = userList.splice(i, 1)[0];
    await updateUserList(userList);
    return id;
  }
}

/**
 * Retrieve the url of the video and send to the user as an attachment.
 * @param {TurnContext} turnContext Current turn context.
 * @param {String} videoName The video name to be sent.
 */
async function sendVideo(turnContext, videoName) {
  // Send the video back to the user.
  const signedUrl = await getVideoUrl(videoName);
  const thumbnailUrl = await getThumbnailUrl(videoName);
  const reply = { type: ActivityTypes.Message };
  reply.attachments = [await getInternetAttachment(signedUrl, thumbnailUrl)];
  return await turnContext.sendActivity(reply);

  // const role = "Student";
  // const { videoUrl } = await getUserProfile(videoName.split(".")[0], role);
  // return await turnContext.sendActivity(videoUrl);
}

/**
 * Retrieve the url of the image and send to the user as an attachment.
 * @param {TurnContext} turnContext Current turn context.
 * @param {String} imageName The image name to be sent.
 */
async function sendImage(turnContext, imageName) {
  // Send the image to the user.
  const imageUrl = await getThumbnailUrl(imageName);
  const reply = { type: ActivityTypes.Message };
  reply.attachments = [
    {
      contentType: "image/jpeg",
      contentUrl: imageUrl,
    },
  ];
  await turnContext.sendActivity(reply);
}

/**
 * Get the attachment from internet.
 * @param {String} signedUrl The signed url of the video.
 * @param {String} thumbnailUrl The thumbnail url of the video.
 */
async function getInternetAttachment(signedUrl, thumbnailUrl) {
  return {
    contentType: "video/mp4",
    contentUrl: signedUrl,
    thumbnailUrl: thumbnailUrl,
  };
}

exports.sendVideo = sendVideo;
exports.sendImage = sendImage;
exports.getVideoOwner = getVideoOwner;
