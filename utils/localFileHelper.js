const fs = require("fs");
const util = require("util");
const axios = require("axios");
const xlsx = require("xlsx");

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);
const USER_ACCOUNT = "user-account.json";

/**
 * Get the user email address and role from the unit enrolment list.
 * @param {String} id Current user id.
 */
function getUserDetails(id) {
  try {
    var buffer = fs.readFileSync(__dirname + "/whitelist.xlsx");
    var workbook = xlsx.read(buffer, { type: "buffer" });
    var sheetNameList = workbook.SheetNames;
    var xlData = xlsx.utils.sheet_to_json(workbook.Sheets[sheetNameList[0]]);

    for (var i = 0; i < xlData.length; i++) {
      if (xlData[i].STUDENT_CODE.toString() === id)
        return {
          email: xlData[i].EMAIL_ADDRESS,
          role: xlData[i].ROLE,
          name: xlData[i].FIRST_NAME,
        };
    }
    return { email: null, role: null, name: null };
  } catch (error) {
    console.error(error);
    return { email: null, role: null, name: null };
  }
}

/**
 * Check whether the current user has registered by searching the channel with user id.
 * @param {String} channel The channel used by the student.
 * @param {String} userId The user id from the current channel.
 */
async function getUserAccount(channel, userId) {
  var allUserAccount = await getAllUserAccount();

  if (allUserAccount.hasOwnProperty(channel)) {
    if (allUserAccount[channel].hasOwnProperty(userId))
      return allUserAccount[channel][userId]["studentId"];
  }
  return null;
}

/**
 * Update the user account information and write to the .json file.
 * @param {String} studentId Current student id.
 * @param {String} channel The channel used by the student.
 * @param {String} userId The user id from the current channel.
 */
async function setUserAccount(studentId, channel, userId) {
  var allUserAccount = await getAllUserAccount();

  // Store the student id under the current social media channel with the user id.
  if (!allUserAccount.hasOwnProperty(channel)) allUserAccount[channel] = {};
  allUserAccount[channel][userId] = { studentId: studentId };

  try {
    const jsonString = JSON.stringify(allUserAccount);
    await writeFile(USER_ACCOUNT, jsonString);
  } catch (err) {
    console.error(err);
  }
}

/**
 * Read all user account details from the .json file.
 */
async function getAllUserAccount() {
  try {
    const jsonString = await readFile(USER_ACCOUNT, "utf8");
    return JSON.parse(jsonString);
  } catch (err) {
    console.error(err);
  }
}

/**
 * Remove a specific file from the local directory.
 * @param {String} localFileName The local file name to be deleted.
 */
async function removeFile(localFileName) {
  try {
    fs.unlinkSync(localFileName);
  } catch (error) {
    console.error(error);
  }
}

/**
 * Download a specific attachment and store it to the local directory.
 * @param {String} url The attachment url to be downloaded.
 * @param {String} userId Current user id.
 * @param {String} contentType The attachment content type.
 */
async function getAttachment(url, userId, contentType) {
  // Local file path for the bot to save the attachment.
  const localFileName = `./${userId}.${contentType}`;

  try {
    const response = await axios.get(url, { responseType: "arraybuffer" });

    // If user uploads JSON file, this prevents it from being written as
    // "{"type":"Buffer","data":[123,13,10,32,32,34,108...".
    if (response.headers["content-type"] === "application/json") {
      response.data = JSON.parse(response.data, (key, value) => {
        return value && value.type === "Buffer"
          ? Buffer.from(value.data)
          : value;
      });
    }

    // Write the file to the local directory.
    await writeFile(localFileName, response.data, (fsError) => {
      if (fsError) throw fsError;
    });
  } catch (error) {
    console.error(error);
    return null;
  }

  // If no error, then return the localFileName.
  return localFileName;
}

/**
 * Get the channel name from the current turn context.
 * @param {TurnContext} turnContext Current turn context.
 */
function getChannel(turnContext) {
  var channel;
  if (turnContext.channelId === "directline")
    channel = turnContext.activity.from.name;
  else channel = turnContext.activity.channelId;
  return channel.toUpperCase();
}

exports.getUserAccount = getUserAccount;
exports.getUserDetails = getUserDetails;
exports.setUserAccount = setUserAccount;
exports.removeFile = removeFile;
exports.getAttachment = getAttachment;
exports.getChannel = getChannel;
