class Ranking {
  /**
   * @param {Array} audienceRankList
   * @param {Array} visualRankList
   * @param {Array} knowledgeRankList
   * @param {Array} rankProviders
   * @param {Double} avgAudience
   * @param {Double} avgVisual
   * @param {Double} avgKnowledge
   */
  constructor(
    audienceRankList,
    visualRankList,
    knowledgeRankList,
    rankProviders,
    avgAudience,
    avgVisual,
    avgKnowledge
  ) {
    this.audienceRankList = audienceRankList;
    this.visualRankList = visualRankList;
    this.knowledgeRankList = knowledgeRankList;
    this.rankProviders = rankProviders;
    this.avgAudience = avgAudience;
    this.avgVisual = avgVisual;
    this.avgKnowledge = avgKnowledge;
  }
}

module.exports.Ranking = Ranking;
