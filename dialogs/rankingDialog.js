const {
  ComponentDialog,
  TextPrompt,
  WaterfallDialog,
} = require("botbuilder-dialogs");

const {
  postRanking,
  getUserProfile,
  updateReplyFlag,
} = require("../firebase/firebaseHelper");
const { getChannel } = require("../utils/localFileHelper");
const { checkLuisConfirm } = require("../luis/luisHelper");
const { getVideoOwner, sendVideo } = require("../utils/videoHelper");
const { MAX_FEEDBACK_NO, MAX_FEEDBACK_TUTOR } = process.env;

class RankingDialog extends ComponentDialog {
  constructor() {
    super("RankingDialog");

    // Define the main dialog and its related components.
    this.addDialog(new TextPrompt("ConfirmTextPrompt", this.thumbValidator));
    this.addDialog(new TextPrompt("RankingTextPrompt", this.rankingValidator));

    this.addDialog(
      new WaterfallDialog("MainWaterfallDialog", [
        this.getVideoStep.bind(this),
        this.confirmNextStep.bind(this),
      ])
    );

    this.addDialog(
      new WaterfallDialog("GetRankDialog", [
        this.bestVisualStep.bind(this),
        this.worstVisualStep.bind(this),
        this.bestAudienceStep.bind(this),
        this.worstAudienceStep.bind(this),
        this.bestKnowledgeStep.bind(this),
        this.worstKnowledgeStep.bind(this),
        this.summaryStep.bind(this),
      ])
    );

    this.initialDialogId = "MainWaterfallDialog";
    this.getRankDialogId = "GetRankDialog";
  }

  /**
   * Prompts the user for the retrieving the next video to be reviewed.
   * @param {WaterfallStepContext} stepContext
   */
  async getVideoStep(stepContext) {
    const id = stepContext.options.id;
    stepContext.values.id = id;
    const role = stepContext.options.role;
    stepContext.values.role = role;

    const { name, providedNo } = await getUserProfile(id, role);
    stepContext.values.name = name;
    const ignoreLimit = stepContext.options.ignoreLimit;
    stepContext.values.ignoreLimit = ignoreLimit;

    // Check whether the user has reached the feedback limit.
    const max = role === "Student" ? MAX_FEEDBACK_NO : MAX_FEEDBACK_TUTOR;
    if (providedNo >= max && !ignoreLimit) {
      await stepContext.context.sendActivity(
        `Sit back and relax! You have already completed all the feedback tasks 😝`
      );
      return await stepContext.endDialog();
    }

    // Check confirm with luis `Communication.Confirm` intent.
    if (!stepContext.options.retry) {
      const confirm = await checkLuisConfirm(stepContext.context);
      if (!confirm) {
        await stepContext.context.sendActivity(
          "/:strong when you want the first video."
        );
        return await stepContext.endDialog();
      }
    }

    // Get or generate the video owner list.
    let videoOwnerList;
    if (stepContext.options.videoOwnerList) {
      videoOwnerList = stepContext.options.videoOwnerList;
      if (ignoreLimit && !stepContext.options.retry)
        stepContext.values.originalList = [...videoOwnerList];
      else stepContext.values.originalList = stepContext.options.originalList;
    } else {
      videoOwnerList = await getVideoOwnerList(id, role, providedNo);
      stepContext.values.originalList = [...videoOwnerList];
    }

    // console.log("Original List: ", videoOwnerList);

    await updateReplyFlag(id, role, false);
    const videoRef = getVideoReference(videoOwnerList.length);

    if (videoRef !== "C")
      await stepContext.context.sendActivity(
        `I'm sending you ${videoRef} now. It may take up to 5 mins to get to you. Watch it as many times as you like, then /:strong to see the next one.`
      );
    else
      await stepContext.context.sendActivity(
        `I'm sending you ${videoRef} now to watch. It may take up to 5 mins to get to you. Now you have seen all three, I’ll ask you to rank them...`
      );

    // Slice out the current video owner and send the video to the user.
    const videoOwner = videoOwnerList.splice(0, 1);
    stepContext.values.videoOwnerList = videoOwnerList;
    await sendVideo(stepContext.context, `${videoOwner}.mp4`);

    // Only set the reply flag back to true if the channel is not directline.
    const channel = getChannel(stepContext.context);
    if (channel !== "DIRECTLINE") await updateReplyFlag(id, role, true);

    // If the video sent is not the last video, restart the current step for retrieving more videos.
    if (videoRef !== "C") {
      return await stepContext.prompt("ConfirmTextPrompt", {
        // prompt: `Video ${videoRef} will be sent to shortly. Please allow a maximum 5 minutes before you see the video. Watch it as many times as you like, then give me a thumbs up for the next one.`,
        retryPrompt: "/:strong when you want the next video.",
      });
    }

    return await stepContext.replaceDialog(this.getRankDialogId, {
      id: stepContext.values.id,
      name: stepContext.values.name,
      role: stepContext.values.role,
      ignoreLimit: stepContext.values.ignoreLimit,
      originalList: stepContext.values.originalList,
    });
  }

  /**
   * Check whether the user message is confirmation. If so, restart the initial dialog
   * for sending another video.
   * @param {WaterfallStepContext} stepContext
   */
  async confirmNextStep(stepContext) {
    return await stepContext.replaceDialog(this.initialDialogId, {
      retry: true,
      id: stepContext.values.id,
      role: stepContext.values.role,
      ignoreLimit: stepContext.values.ignoreLimit,
      originalList: stepContext.values.originalList,
      videoOwnerList: stepContext.values.videoOwnerList,
    });
  }

  /**
   * Prompts the user for the visual content ranking (best).
   * @param {WaterfallStepContext} stepContext
   */
  async bestVisualStep(stepContext) {
    stepContext.values.id = stepContext.options.id;
    stepContext.values.name = stepContext.options.name;
    stepContext.values.role = stepContext.options.role;
    stepContext.values.ignoreLimit = stepContext.options.ignoreLimit;
    stepContext.values.originalList = stepContext.options.originalList;

    // console.log("Original List: ", stepContext.values.originalList);

    const promptOptions = {
      prompt:
        "Which of the videos (A, B, C) do you consider is MOST effective at using visuals to engage the viewer?",
      retryPrompt:
        "Oops, please reply with A, B, or C for your ranking and avoid selecting the same one as previously ranked 😵",
    };
    return await stepContext.prompt("RankingTextPrompt", promptOptions);
  }

  /**
   * Prompts the user for the visual content ranking (worst).
   * @param {WaterfallStepContext} stepContext
   */
  async worstVisualStep(stepContext) {
    const index = stepContext.result.toLowerCase().charCodeAt(0) - 97;
    const visualRank = { 1: stepContext.values.originalList[index] };
    stepContext.values.visualRank = visualRank;

    const promptOptions = {
      prompt:
        "Which of the videos (A, B, C) do you consider is LEAST effective at using visuals to engage the viewer?",
      retryPrompt:
        "Oops, please reply with A, B, or C for your ranking and avoid selecting the same one as previously ranked 😵",
      duplicateRank: stepContext.result,
    };
    return await stepContext.prompt("RankingTextPrompt", promptOptions);
  }

  /**
   * Prompts the user for the audience communication ranking (best).
   * @param {WaterfallStepContext} stepContext
   */
  async bestAudienceStep(stepContext) {
    const index = stepContext.result.toLowerCase().charCodeAt(0) - 97;
    stepContext.values.visualRank[3] = stepContext.values.originalList[index];

    // Calculate and assign the middle ranking video.
    const visualRank = stepContext.values.visualRank;
    for (var i = 0; i < stepContext.values.originalList.length; i++) {
      const videoOwner = stepContext.values.originalList[i];
      if (videoOwner !== visualRank[1] && videoOwner !== visualRank[3]) {
        stepContext.values.visualRank[2] = videoOwner;
        break;
      }
    }

    const promptOptions = {
      prompt:
        "Which of the videos (A, B, C) communicates the paper MOST effectively?",
      retryPrompt:
        "Oops, please reply with A, B, or C for your ranking and avoid selecting the same one as previously ranked 😵",
    };
    return await stepContext.prompt("RankingTextPrompt", promptOptions);
  }

  /**
   * Prompts the user for the audience communication ranking (worst).
   * @param {WaterfallStepContext} stepContext
   */
  async worstAudienceStep(stepContext) {
    const index = stepContext.result.toLowerCase().charCodeAt(0) - 97;
    const audienceRank = { 1: stepContext.values.originalList[index] };
    stepContext.values.audienceRank = audienceRank;

    const promptOptions = {
      prompt:
        "Which of the videos (A, B, C) communicates the paper LEAST effectively?",
      retryPrompt:
        "Oops, please reply with A, B, or C for your ranking and avoid selecting the same one as previously ranked 😵",
      duplicateRank: stepContext.result,
    };
    return await stepContext.prompt("RankingTextPrompt", promptOptions);
  }

  async bestKnowledgeStep(stepContext) {
    const index = stepContext.result.toLowerCase().charCodeAt(0) - 97;
    stepContext.values.audienceRank[3] = stepContext.values.originalList[index];

    // Calculate and assign the middle ranking video.
    for (var i = 0; i < stepContext.values.originalList.length; i++) {
      const videoOwner = stepContext.values.originalList[i];
      if (
        videoOwner !== stepContext.values.audienceRank[1] &&
        videoOwner !== stepContext.values.audienceRank[3]
      ) {
        stepContext.values.audienceRank[2] = videoOwner;
        break;
      }
    }

    const promptOptions = {
      prompt:
        "Which video (A, B, C) MOST appropriately responds to the audience’s pre-existing knowledge?",
      retryPrompt:
        "Oops, please reply with A, B, or C for your ranking and avoid selecting the same one as previously ranked 😵",
    };
    return await stepContext.prompt("RankingTextPrompt", promptOptions);
  }

  async worstKnowledgeStep(stepContext) {
    const index = stepContext.result.toLowerCase().charCodeAt(0) - 97;
    const knowledgeRank = { 1: stepContext.values.originalList[index] };
    stepContext.values.knowledgeRank = knowledgeRank;

    const promptOptions = {
      prompt:
        "Which video (A, B, C) LEAST appropriately responds to the audience’s pre-existing knowledge?",
      retryPrompt:
        "Oops, please reply with A, B, or C for your ranking and avoid selecting the same one as previously ranked 😵",
      duplicateRank: stepContext.result,
    };
    return await stepContext.prompt("RankingTextPrompt", promptOptions);
  }

  /**
   * Final step for summarzie the dialog.
   * @param {WaterfallStepContext} stepContext
   */
  async summaryStep(stepContext) {
    const index = stepContext.result.toLowerCase().charCodeAt(0) - 97;
    stepContext.values.knowledgeRank[3] =
      stepContext.values.originalList[index];

    // Calculate and assign the middle ranking video.
    for (var i = 0; i < stepContext.values.originalList.length; i++) {
      const videoOwner = stepContext.values.originalList[i];
      if (
        videoOwner !== stepContext.values.knowledgeRank[1] &&
        videoOwner !== stepContext.values.knowledgeRank[3]
      ) {
        stepContext.values.knowledgeRank[2] = videoOwner;
        break;
      }
    }

    // Post ranking into Firebase.
    const id = stepContext.values.id;
    const role = stepContext.values.role;
    const name = stepContext.values.name;
    const videoOwnerList = stepContext.values.originalList;
    const visualRank = stepContext.values.visualRank;
    const audienceRank = stepContext.values.audienceRank;
    const knowledgeRank = stepContext.values.knowledgeRank;
    await postRanking(
      id,
      role,
      videoOwnerList,
      audienceRank,
      visualRank,
      knowledgeRank
    );

    var message;
    if (stepContext.values.ignoreLimit) {
      message = `Thanks for all your hard work, ${name}. We have a couple more videos that we want your feedback on. If you are interested, try to send a thumbs up again to review more videos!`;
    } else {
      const { providedNo } = await getUserProfile(id, role);
      const max = role === "Student" ? MAX_FEEDBACK_NO : MAX_FEEDBACK_TUTOR;
      if (providedNo < max)
        message = `Thanks for all your hard work, ${name}. We have a couple more videos that we want your feedback on. Chat with me again whenever you are available!`;
      else
        message = `You’re all done, ${name}! I’ll let you know when ranking results come in for your video...`;
    }
    await stepContext.context.sendActivity(message);
    return await stepContext.endDialog();
  }

  /**
   * Thumb Prompt Validator to check whether the user replies a confirm message.
   * @param {PromptValidatorContext} promptContext
   */
  async thumbValidator(promptContext) {
    const confirm = await checkLuisConfirm(promptContext.context);
    return promptContext.recognized.succeeded && confirm;
  }

  /**
   * Ranking Prompt Validator to check whether the ranking is valid.
   * @param {PromptValidatorContext} promptContext
   */
  async rankingValidator(promptContext) {
    if (!promptContext.recognized.succeeded) return false;
    var duplicate;
    if (promptContext.options.duplicateRank)
      duplicate = promptContext.options.duplicateRank.toUpperCase();

    const str = promptContext.recognized.value.toUpperCase();
    return (
      str.length === 1 &&
      ["A", "B", "C"].indexOf(str) >= 0 &&
      (!duplicate || str !== duplicate)
    );
  }
}

/**
 * Get the video owner list based the ranking provided number.
 * @param {String} id
 * @param {String} role
 * @param {Integer} index
 */
async function getVideoOwnerList(id, role, index) {
  var videoOwnerList = [];
  for (i = 0; i <= 2; i++) {
    const videoOwner = await getVideoOwner(id, role, index * 3 + i);
    videoOwnerList.push(videoOwner);
  }
  return videoOwnerList;
}

/**
 * Get the video reference character (A, B, or C).
 * @param {Integer} length
 */
function getVideoReference(length) {
  switch (length) {
    case 3:
      return "A";
    case 2:
      return "B";
    case 1:
      return "C";
  }
}

module.exports.RankingDialog = RankingDialog;
