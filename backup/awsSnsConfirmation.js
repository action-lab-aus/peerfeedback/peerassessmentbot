var handleSubscriptionResponse = function (error, response) {
  if (!error && response.statusCode == 200) {
    console.log("Yes! We have accepted the confirmation from AWS.");
  } else {
    throw new Error(`Unable to subscribe to given URL`);
  }
};

server.post(
  "/api/aws-sns",
  bodyParser.urlencoded({
    extended: true,
  }),
  bodyParser.text(),
  async (req, res) => {
    let payload = JSON.parse(req.body);
    try {
      if (req.header("x-amz-sns-message-type") === "SubscriptionConfirmation") {
        const url = payload.SubscribeURL;
        await request(url, handleSubscriptionResponse);
      } else if (req.header("x-amz-sns-message-type") === "Notification") {
        console.log(payload);
      } else {
        throw new Error(`Invalid message type ${payload.Type}`);
      }
    } catch (err) {
      console.error(err);
      res.status(500).send("Oops");
    }
    res.send("Ok");
  }
);
