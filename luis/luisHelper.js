const { getLuisResponse } = require("./luisEndpoint");
const {
  getCurrentStage,
  getStudentVideo,
  getUserProfile,
} = require("../firebase/firebaseHelper");

/**
 * Check whether the user intent is Communication.Confirm.
 * @param {TurnContext} turnContext Current turn context.
 */
async function checkLuisConfirm(turnContext) {
  const { intent, score } = await getIntentAndScore(turnContext);
  return score >= 0.8 ? intent === "Utilities.Confirm" : false;
}

/**
 * Check whether the user intent is Utilities.Help.
 * @param {TurnContext} turnContext Current turn context.
 */
async function checkLuisHelp(turnContext) {
  const { intent, score } = await getIntentAndScore(turnContext);
  return score >= 0.8 ? intent === "Utilities.Help" : false;
}

/**
 * Get the intent and score by calling the luis endpoint.
 * @param {TurnContext} turnContext Current turn context.
 */
async function getIntentAndScore(turnContext) {
  if (turnContext.activity.type === "message" && turnContext.activity.text) {
    try {
      // Retrieve the text message from the turnContext.
      const msg = turnContext.activity.text;
      // Get luis response by forwarding the user message.
      const luisResponse = JSON.parse(await getLuisResponse(msg));

      // Calculate the top scoring intent with the corresponding score.
      if (luisResponse.prediction) {
        const intent = luisResponse.prediction.topIntent;
        const score = luisResponse.prediction.intents[intent].score;
        // console.log(`Intent: ${intent} Score: ${score}`);

        // If the score is high enough, then parse the response and fulfill user request.
        return { intent: intent, score: score };
      } else return { intent: "None", score: 0.0 };
    } catch (error) {
      console.error(error);
      return { intent: "None", score: 0.0 };
    }
  }
  return { intent: "None", score: 0.0 };
}

/**
 * Get the help message according to the current stage.
 * @param {String} id Current user id.
 * @param {String} role Current user role.
 */
async function getHelpMessage(id, role) {
  const stage = await getCurrentStage();
  var message = `We are currently in the ${stage} stage. `;

  switch (stage) {
    // Check whether the student has registered or not.
    case "Registration":
      message += id
        ? "You have successfully registered."
        : "Please share a photo of your Monash ID card for registration.";
      break;

    // Check whether the student has submitted the video or not.
    case "Upload":
      if (role === "Student") {
        let videoExistence = await getStudentVideo(id);
        const { videoUrl } = await getUserProfile(id, role);
        videoExistence = videoExistence && videoUrl;
        message +=
          id && videoExistence
            ? "You have successfully submitted your video."
            : "Please share a Google Drive/Spark/OneDrive Personal video link for submission.";
      }
      break;

    // Check the ranking status (no. of ranking provided and received).
    case "Ranking":
      if (!id)
        message +=
          "You have not registered, please contact your tutor/admin to fix the issue.";
      else {
        const { providedNo, receivedNo } = await getUserProfile(id, role);
        if (role === "Student")
          message += `You have provided ${providedNo} and received ${receivedNo} feedback for now.`;
        else message += `You have provided ${providedNo} feedback for now.`;
      }
      break;

    // Provide more information regarding the reallocation stage.
    case "Reallocate":
      if (role === "Student")
        message +=
          "We still have videos which require some more feedback. Simply send a thumbs up to reserve a spot.";
      break;

    // Check the no. of ranking received.
    case "Review":
      if (role === "Student") {
        if (!id)
          message +=
            "You have not registered, please contact your tutor/admin to fix the issue.";
        else {
          const { receivedNo } = await getUserProfile(id, role);
          message += `You have received ${receivedNo} feedback for now. Send me a thumbs up when you want your feedback.`;
        }
      }
      break;

    // Provide more information regarding the summary stage.
    case "Summary":
      message +=
        "If you disagreed with your ranking in the previous stage, you will be notified when the reviewer makes a detailed comment.";
      break;
  }
  return message;
}

exports.checkLuisConfirm = checkLuisConfirm;
exports.checkLuisHelp = checkLuisHelp;
exports.getHelpMessage = getHelpMessage;
