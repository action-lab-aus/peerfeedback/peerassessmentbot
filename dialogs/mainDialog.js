const {
  ComponentDialog,
  DialogSet,
  DialogTurnStatus,
  WaterfallDialog,
} = require("botbuilder-dialogs");

const {
  getMonashId,
  getStudentVideo,
  getUserProfile,
  getCurrentStage,
  getRankDetails,
  getRandomVideos,
  updateAssessStatus,
} = require("../firebase/firebaseHelper");
const { getChannel } = require("../utils/localFileHelper");
const { RegistrationDialog } = require("../dialogs/registrationDialog");
const { VideoDialog } = require("../dialogs/videoDialog");
const { RankingDialog } = require("../dialogs/rankingDialog");
const { ReviewDialog } = require("../dialogs/reviewDialog");
const { DetailDialog } = require("../dialogs/detailDialog");

const MAIN_DIALOG = "MAIN_DIALOG";
const WATERFALL_DIALOG = "WATERFALL_DIALOG";

class MainDialog extends ComponentDialog {
  constructor(userState) {
    super(MAIN_DIALOG);
    this.userState = userState;

    this.addDialog(new RegistrationDialog());
    this.addDialog(new VideoDialog());
    this.addDialog(new RankingDialog());
    this.addDialog(new ReviewDialog());
    this.addDialog(new DetailDialog());

    this.addDialog(
      new WaterfallDialog(WATERFALL_DIALOG, [
        this.initialStep.bind(this),
        this.finalStep.bind(this),
      ])
    );

    this.initialDialogId = WATERFALL_DIALOG;
  }

  /**
   * The run method handles the incoming activity (in the form of a TurnContext) and passes
   * it through the dialog system. If no dialog is active, it will start the default dialog.
   * @param {*} turnContext
   * @param {*} accessor
   */
  async run(turnContext, accessor) {
    const dialogSet = new DialogSet(accessor);
    dialogSet.add(this);

    const dialogContext = await dialogSet.createContext(turnContext);
    const results = await dialogContext.continueDialog();
    if (results.status === DialogTurnStatus.empty) {
      await dialogContext.beginDialog(this.id);
    }
  }

  /**
   * Initial step of the main dialog is to check the current stage and enter the
   * corresponding waterfall dialog.
   * @param {WaterfallStepContext} stepContext
   */
  async initialStep(stepContext) {
    // Retrieve the user id and channel from the current turnContext.
    const userId = stepContext.context.activity.from.id;
    const channel = getChannel(stepContext.context);

    // Retrieve monash id, video existence and current stage from Firestore.
    let { id, role } = await getMonashId(channel, userId);

    // For WebChat user, start the registration dialog if id is null
    if (channel === "EMULATOR" || channel === "WEBCHAT") {
      if (!id)
        return await stepContext.beginDialog("RegistrationDialog", { id: id });
    }

    let videoExistence = false;
    if (role === "Student") {
      videoExistence = await getStudentVideo(id);
      const { videoUrl } = await getUserProfile(id, role);
      videoExistence = videoExistence && videoUrl;
    }

    const stage = await getCurrentStage();

    // Check the Summary stage.
    if (stage === "Summary") {
      var sent = await sendNotRegMsg(stepContext, id, role, videoExistence);
      if (sent) return await stepContext.endDialog();

      const { rankDetails, finished } = await getRankDetails(id, role);
      sent = await sendNoDetailMsg(stepContext, rankDetails, finished);
      if (sent) return await stepContext.endDialog();

      return await stepContext.beginDialog("DetailDialog", {
        id: id,
        role: role,
      });
    }

    // Check the Review stage.
    if (stage === "Review") {
      const sent = await sendNotRegMsg(stepContext, id, role, videoExistence);
      if (sent) return await stepContext.endDialog();

      // If the current user is tutor, continue with the ranking stage.
      if (role !== "Student") {
        return await stepContext.beginDialog("RankingDialog", {
          id: id,
          role: role,
        });
      }

      return await stepContext.beginDialog("ReviewDialog", { id: id });
    }

    // Check the Reallocate stage.
    if (stage === "Reallocate") {
      var sent = await sendNotRegMsg(stepContext, id, role, videoExistence);
      if (sent) return await stepContext.endDialog();

      // If the current user is tutor, continue with the ranking stage.
      if (role !== "Student") {
        return await stepContext.beginDialog("RankingDialog", {
          id: id,
          role: role,
        });
      }

      // Get random video from Firestore.
      const docs = await getRandomVideos(id);

      // If docs is empty, then the video owner list contains the current student after 3
      // retries, just simply ask the student to wait for a while.
      if (!docs) {
        await stepContext.context.sendActivity(
          "We are currently reallocating the video that you have submitted, please try again later."
        );
        return await stepContext.endDialog();
      }

      // If remaining video to be reviewed is less than 3, then all videos have been fully reviewed.
      sent = await sendNoFeedbackMsg(stepContext, docs);
      if (sent) return await stepContext.endDialog();

      // Otherwise, try to update the assess status.
      const success = await updateAssessStatus(docs);
      sent = await sendUpdateErrorMsg(stepContext, success);
      if (sent) return await stepContext.endDialog();

      // Start the feedback dialog with a fixed video owner reference.
      var videoOwnerList = [docs[0].id, docs[1].id, docs[2].id];
      return await stepContext.beginDialog("RankingDialog", {
        id: id,
        role: role,
        videoOwnerList: videoOwnerList,
        ignoreLimit: true,
      });
    }

    // Check the Ranking stage.
    if (stage === "Ranking") {
      const sent = await sendNotRegMsg(stepContext, id, role, videoExistence);
      if (sent) return await stepContext.endDialog();

      return await stepContext.beginDialog("RankingDialog", {
        id: id,
        role: role,
      });
    }

    // Check the Upload stage.
    if (stage === "Upload") {
      if (!id)
        return await stepContext.beginDialog("RegistrationDialog", { id: id });

      // If the current user is tutor, skip the current stage.
      if (role !== "Student") {
        await stepContext.context.sendActivity(
          `Tutors do not need to participate in the upload stage! You will be prompted to provide feedback at a later stage...`
        );
        return await stepContext.endDialog();
      }

      // If video has been uploaded, then end the current dialog.
      if (videoExistence) {
        await stepContext.context.sendActivity(
          `You have already uploaded a video 😝 We will let you know when there is something else to do.`
        );
        return await stepContext.endDialog();
      }

      // Otherwise, start upload video dialog.
      return await stepContext.beginDialog("VideoDialog", { id: id });
    }

    // Check the Registration stage.
    if (stage === "Registration")
      return await stepContext.beginDialog("RegistrationDialog", { id: id });
  }

  /**
   * Final step of the main dialog.
   * @param {WaterfallStepContext} stepContext
   */
  async finalStep(stepContext) {
    return await stepContext.endDialog();
  }
}

/**
 * Check whether the user has registered or not. If not, send back the error message.
 * @param {WaterfallStepContext} stepContext
 * @param {String} id
 * @param {String} role
 * @param {Boolean} videoExistence
 */
async function sendNotRegMsg(stepContext, id, role, videoExistence) {
  if (role === "Tutor") return false;

  if (!id && !videoExistence) {
    await stepContext.context.sendActivity(
      "Unfortunately, you were late to the party and are not able to join in with the feedback process 😞"
    );
    return true;
  } else if (id && !videoExistence) {
    await stepContext.context.sendActivity(
      "Unfortunately, you did not send me your video in time, so have missed out on getting feedback 😞"
    );
    return true;
  }

  // If index is null, then the user has submitted the video after the deadline (students might still be in
  // the upload stage and can finish the conversation). Hence, return error message back to the user.
  const { index } = await getUserProfile(id, role);
  if (!index) {
    await stepContext.context.sendActivity(
      "Unfortunately, you did not send me your video in time, so have missed out on getting feedback 😞"
    );
    return true;
  }

  return false;
}

/**
 * Check whether the student need to provide more ranking details. If not, send back the
 * error message.
 * @param {WaterfallStepContext} stepContext
 * @param {Array} rankDetails
 * @param {Boolean} finished
 */
async function sendNoDetailMsg(stepContext, rankDetails, finished) {
  if (rankDetails.size === 0 || finished) {
    await stepContext.context.sendActivity(
      "I seem to have everything I need from you for the moment!"
    );
    return true;
  }
  return false;
}

/**
 * Check whether all 3 random documents generated are valid. If not, send back the error message.
 * @param {WaterfallStepContext} stepContext
 * @param {Array} docs
 */
async function sendNoFeedbackMsg(stepContext, docs) {
  // If no document retrieved, all assessments have been recorded.
  if (!docs[0] || !docs[1] || !docs[2]) {
    await stepContext.context.sendActivity(
      "Turns out, all videos have now been ranked.Thanks for offering!"
    );
    return true;
  }
  return false;
}

/**
 * Check whether the 3 random documents generated contains the current student's video.
 * If yes, send back the error message.
 * @param {WaterfallStepContext} stepContext
 * @param {String} studentId
 * @param {Array} docs
 */
async function sendOwnVideoMsg(stepContext, studentId, docs) {
  // If current video belongs to the user requested, ask the user to try again
  if (
    docs[0].id === studentId ||
    docs[1].id === studentId ||
    docs[2].id === studentId
  ) {
    await stepContext.context.sendActivity(
      "We are currently reallocating the video that you have submitted, please try again later."
    );
    return true;
  }
  return false;
}

/**
 * Check whether the assess status has been updated successfully. If not, send back the error message.
 * @param {WaterfallStepContext} stepContext
 * @param {Boolean} success
 */
async function sendUpdateErrorMsg(stepContext, success) {
  if (!success) {
    // Throw error when trying to update/delete the videoOwner reference which has
    // already been removed, simply end dialog and ask user to try again.
    await stepContext.context.sendActivity(
      "It seems that someone else has reserved the spot for reviewing the video 😝 Try again later to see whether there are more videos available!"
    );
    return true;
  }
  return false;
}

module.exports.MainDialog = MainDialog;
