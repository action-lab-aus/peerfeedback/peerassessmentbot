const {
  ComponentDialog,
  TextPrompt,
  WaterfallDialog,
} = require("botbuilder-dialogs");

const {
  getUserProfile,
  getRankReceived,
  updateReplyFlag,
  postReviewFlag,
  postComment,
} = require("../firebase/firebaseHelper");
const { Ranking } = require("../models/ranking");
const { sendVideo } = require("../utils/videoHelper");
const { getChannel } = require("../utils/localFileHelper");
const { checkLuisConfirm } = require("../luis/luisHelper");
const ROLE = "Student";

class ReviewDialog extends ComponentDialog {
  constructor() {
    super("ReviewDialog");

    // Define the main dialog and its related components.
    this.addDialog(new TextPrompt("MoreTextPrompt"));
    this.addDialog(new TextPrompt("OptionTextPrompt", this.optionValidator));
    this.addDialog(new TextPrompt("ConfirmTextPrompt", this.confirmValidator));

    this.addDialog(
      new WaterfallDialog("MainWaterfallDialog", [
        this.getRankSummaryStep.bind(this),
        this.confirmOptionStep.bind(this),
      ])
    );

    this.addDialog(
      new WaterfallDialog("RankDetailDialog", [
        this.getDetailStep.bind(this),
        this.confirmMoreStep.bind(this),
      ])
    );

    this.addDialog(
      new WaterfallDialog("RankOptionDialog", [
        this.rankOptionStep.bind(this),
        this.summaryStep.bind(this),
      ])
    );

    this.initialDialogId = "MainWaterfallDialog";
    this.rankDetailDialogId = "RankDetailDialog";
    this.rankOptionDialogId = "RankOptionDialog";
  }

  /**
   * Prompts the user to choose the ranking feedback for review.
   * @param {WaterfallStepContext} stepContext
   */
  async getRankSummaryStep(stepContext) {
    const id = stepContext.options.id;
    stepContext.values.id = id;
    const { name, receivedNo } = await getUserProfile(id, ROLE);
    stepContext.values.name = name;

    // Get the ranking details.
    var ranking;
    var prompt;
    if (!stepContext.options.ranking) {
      const rankList = await getRankReceived(id);

      if (receivedNo === 0) {
        await stepContext.context.sendActivity(
          "For some reason, your videos didn't receive any feedback... 😕"
        );
        return await stepContext.endDialog();
      }

      ranking = calculateAvgRank(rankList, id);
      prompt = `Your video received an average ranking of ${ranking.avgAudience} for audience communication, ${ranking.avgVisual} for visual interest, and ${ranking.avgKnowledge} for responding to audience's pre-existing knowledge. To explore more, enter A for Audience Communication, B for Visual Interest and C for Responding to Audience's Knowledge.`;
    } else {
      ranking = stepContext.options.ranking;
      prompt =
        "Are there any other criteria (A: Audience Communication, B: Visual Interest, C: Responding to Audience's Knowledge) that you want to explore? Reply `No` to stop exploring your feedback.";
    }
    stepContext.values.ranking = ranking;

    // Check confirm with luis `Communication.Confirm` intent.
    if (!stepContext.options.retry) {
      const confirm = await checkLuisConfirm(stepContext.context);
      if (!confirm) {
        await stepContext.context.sendActivity(
          "/:strong when you want the first video."
        );
        return await stepContext.endDialog();
      }
    }

    const promptOptions = {
      prompt: prompt,
      retryPrompt:
        "Oops, I didn't get that. Reply with A, B or C to choose the criteria that you want to explore 😵",
    };

    return await stepContext.prompt("OptionTextPrompt", promptOptions);
  }

  /**
   * Confirm the option selected by the current student.
   * @param {WaterfallStepContext} stepContext
   */
  async confirmOptionStep(stepContext) {
    const option = stepContext.result.toUpperCase();
    if (option === "NO") {
      await stepContext.context.sendActivity(
        "/:strong again when you want more information."
      );
      return await stepContext.endDialog();
    }

    const { requestedList, criteria, avgRanking } = getRankingList(
      option,
      stepContext.values.ranking
    );

    return await stepContext.replaceDialog(this.rankDetailDialogId, {
      id: stepContext.values.id,
      name: stepContext.values.name,
      ranking: stepContext.values.ranking,
      requestedList: requestedList,
      criteria: criteria,
      avgRanking: avgRanking,
    });
  }

  /**
   * Prompts the user for their opinion on the ranking details.
   * @param {WaterfallStepContext} stepContext
   */
  async getDetailStep(stepContext) {
    const id = stepContext.options.id;
    stepContext.values.id = id;
    const name = stepContext.options.name;
    stepContext.values.name = name;
    const ranking = stepContext.options.ranking;
    stepContext.values.ranking = ranking;
    const requestedList = stepContext.options.requestedList;
    stepContext.values.requestedList = requestedList;
    const criteria = stepContext.options.criteria;
    stepContext.values.criteria = criteria;
    const avgRanking = stepContext.options.avgRanking;

    var topVideos;
    if (stepContext.options.topVideos) {
      topVideos = stepContext.options.topVideos;
    } else {
      // If video received straight 1s, restart the initial dialog.
      if (avgRanking === 1) {
        const msg =
          requestedList.length === 1
            ? "1 reviewer"
            : `all ${requestedList.length} reviewers`;
        await stepContext.context.sendActivity(
          `For ${criteria}, your video was ranked top by ${msg}. Congrats!`
        );
        return await stepContext.replaceDialog(this.initialDialogId, {
          retry: true,
          id: stepContext.values.id,
          ranking: stepContext.values.ranking,
        });
      }

      const detailedRanking = getDetailedRanking(requestedList, id);
      await stepContext.context.sendActivity(
        `Ok, for \`${criteria}\`, your video received a rank of ${detailedRanking}. I'll send you a video that ranked higher than yours now. Please allow a maximum of 5 minutes before you see the video.`
      );
      topVideos = getAllTopVideos([...requestedList], id);
    }

    await updateReplyFlag(id, ROLE, false);
    const videoOwner = topVideos.splice(0, 1)[0];
    await sendVideo(stepContext.context, `${videoOwner}.mp4`);
    stepContext.values.topVideos = topVideos;

    // Only set the reply flag back to true if the channel is not directline
    const channel = getChannel(stepContext.context);
    if (channel !== "DIRECTLINE") await updateReplyFlag(id, ROLE, true);

    if (topVideos.length === 0)
      return await stepContext.replaceDialog(this.rankOptionDialogId, {
        id: stepContext.values.id,
        name: stepContext.values.name,
        ranking: stepContext.values.ranking,
        requestedList: stepContext.values.requestedList,
        criteria: stepContext.values.criteria,
      });

    const message =
      "Reply `More` if you want to see more videos ranked higher than yours. /:strong to continue looking at your rankings.";
    return await stepContext.prompt("MoreTextPrompt", message);
  }

  /**
   * Confirm whether the student wants to see more videos.
   * @param {WaterfallStepContext} stepContext
   */
  async confirmMoreStep(stepContext) {
    if (stepContext.result && stepContext.result.toUpperCase() === "MORE") {
      await stepContext.context.sendActivity(
        `Here is another video that ranked higher than yours for ${stepContext.values.criteria}.`
      );
      return await stepContext.replaceDialog(this.rankDetailDialogId, {
        id: stepContext.values.id,
        name: stepContext.values.name,
        ranking: stepContext.values.ranking,
        requestedList: stepContext.values.requestedList,
        criteria: stepContext.values.criteria,
        topVideos: stepContext.values.topVideos,
      });
    }

    return await stepContext.replaceDialog(this.rankOptionDialogId, {
      id: stepContext.values.id,
      name: stepContext.values.name,
      ranking: stepContext.values.ranking,
      requestedList: stepContext.values.requestedList,
      criteria: stepContext.values.criteria,
    });
  }

  /**
   * Prompt user for the ranking option (agree or disagree with the ranking).
   * @param {WaterfallStepContext} stepContext
   */
  async rankOptionStep(stepContext) {
    const id = stepContext.options.id;
    stepContext.values.id = id;
    const name = stepContext.options.name;
    stepContext.values.name = name;
    stepContext.values.ranking = stepContext.options.ranking;
    stepContext.values.requestedList = stepContext.options.requestedList;
    const criteria = stepContext.options.criteria;
    stepContext.values.criteria = criteria;

    // Check the criteria flag, if it is set to be false, then skip asking whether
    // the student agree or disagree with the ranking.
    const { audienceFlag, visualFlag, knowledgeFlag } = await getUserProfile(
      id,
      ROLE
    );
    var flag;
    if (criteria === "Audience Communication") flag = audienceFlag;
    if (criteria === "Visual Interest") flag = visualFlag;
    if (criteria === "Responding to Audience's Knowledge") flag = knowledgeFlag;

    if (flag === false)
      return await stepContext.replaceDialog(this.initialDialogId, {
        retry: true,
        id: stepContext.values.id,
        name: stepContext.values.name,
        ranking: stepContext.values.ranking,
      });

    return await stepContext.prompt(
      "ConfirmTextPrompt",
      "Do you agree with the ranking (yes/no)?"
    );
  }

  /**
   * Final step for summarzie the review dialog.
   * @param {WaterfallStepContext} stepContext
   */
  async summaryStep(stepContext) {
    const confirm = stepContext.result.toUpperCase();
    if (confirm === "NO") {
      // Forwarding message to the reviewer.
      const id = stepContext.values.id;
      const criteria = stepContext.values.criteria;
      const requestedList = stepContext.values.requestedList;
      const rankProviders = stepContext.values.ranking.rankProviders;

      // Update the review flag for the corresponding criteria.
      await postReviewFlag(id, criteria);

      // Post detailed comments for all the reviewers who does not rank the video as 1st.
      for (var i = 0; i < rankProviders.length; i++) {
        const ranking = Object.keys(requestedList[i]).find(
          (key) => requestedList[i][key] === id
        );
        if (ranking !== "1")
          await postComment(
            id,
            rankProviders[i],
            criteria,
            ranking,
            requestedList[i]
          );
      }

      await stepContext.context.sendActivity(
        "I’m going to try to get more information from the reviewers. I’ll let you know when they reply."
      );
    }

    return await stepContext.replaceDialog(this.initialDialogId, {
      retry: true,
      id: stepContext.values.id,
      name: stepContext.values.name,
      ranking: stepContext.values.ranking,
    });
  }

  /**
   * Option Prompt Validator to check whether the option is valid.
   * @param {PromptValidatorContext} promptContext
   */
  async optionValidator(promptContext) {
    if (!promptContext.recognized.succeeded) return false;
    const str = promptContext.recognized.value.toUpperCase();
    return (
      (str.length === 1 && ["A", "B", "C"].indexOf(str) >= 0) || str === "NO"
    );
  }

  /**
   * Confirm Prompt Validator to check whether the user confirms or not.
   * @param {PromptValidatorContext} promptContext
   */
  async confirmValidator(promptContext) {
    const str = promptContext.recognized.value.toUpperCase();
    return str === "YES" || str === "NO";
  }
}

/**
 * Calculate the average ranking the current student received.
 * @param {Array} rankList The ranking received list.
 * @param {String} studentId Current student id.
 */
function calculateAvgRank(rankList, studentId) {
  var sumAudience = 0;
  var sumVisual = 0;
  var sumKnowledge = 0;
  var audienceRankList = [];
  var visualRankList = [];
  var knowledgeRankList = [];
  var rankProviders = [];

  for (i = 0; i < rankList.length; i++) {
    const audienceRank = rankList[i].Audience_Rank;
    audienceRankList.push(audienceRank);
    const visualRank = rankList[i].Visual_Rank;
    visualRankList.push(visualRank);
    const knowledgeRank = rankList[i].Knowledge_Rank;
    knowledgeRankList.push(knowledgeRank);
    const rankProvider = rankList[i].Rank_Provider;
    rankProviders.push(rankProvider);
    sumAudience += parseInt(
      Object.keys(audienceRank).find((key) => audienceRank[key] === studentId)
    );
    sumVisual += parseInt(
      Object.keys(visualRank).find((key) => visualRank[key] === studentId)
    );
    sumKnowledge += parseInt(
      Object.keys(knowledgeRank).find((key) => knowledgeRank[key] === studentId)
    );
  }

  return new Ranking(
    audienceRankList,
    visualRankList,
    knowledgeRankList,
    rankProviders,
    Math.round((sumAudience / rankList.length) * 100) / 100,
    Math.round((sumVisual / rankList.length) * 100) / 100,
    Math.round((sumKnowledge / rankList.length) * 100) / 100
  );
}

/**
 * Get the ranking details based on the option selected.
 * @param {String} option
 * @param {Ranking} ranking
 */
function getRankingList(option, ranking) {
  switch (option) {
    case "A":
      return {
        requestedList: ranking.audienceRankList,
        criteria: "Audience Communication",
        avgRanking: ranking.avgAudience,
      };

    case "B":
      return {
        requestedList: ranking.visualRankList,
        criteria: "Visual Interest",
        avgRanking: ranking.avgVisual,
      };

    case "C":
      return {
        requestedList: ranking.knowledgeRankList,
        criteria: "Responding to Audience's Knowledge",
        avgRanking: ranking.avgKnowledge,
      };
  }
}

/**
 * Get all videos that have a higher ranking than the current user's video.
 * @param {Array} requestedList Requested ranking list.
 * @param {String} studentId Current student id.
 */
function getAllTopVideos(requestedList, studentId) {
  var topVideos = [];
  for (var i = 0; i < requestedList.length; i++) {
    if (requestedList[i]["1"] !== studentId)
      topVideos.push(requestedList[i]["1"]);
    else continue;

    if (requestedList[i]["2"] !== studentId)
      topVideos.push(requestedList[i]["2"]);
    else continue;

    if (requestedList[i]["3"] !== studentId)
      topVideos.push(requestedList[i]["3"]);
    else continue;
  }
  // Remove duplicated items.
  return [...new Set(topVideos)];
}

/**
 * Get detailed ranking string for the requested list.
 * @param {Array} requestedList Requested ranking list.
 * @param {String} studentId Current student id.
 */
function getDetailedRanking(requestedList, studentId) {
  var detailedRanking = "";

  const temp = requestedList.map((item) =>
    Object.keys(item).find((key) => item[key] === studentId)
  );

  if (temp.length === 1) return temp[0];

  for (var i = 0; i < temp.length; i++) {
    detailedRanking += temp[i];
    if (i !== temp.length - 1 && i !== temp.length - 2) detailedRanking += ", ";
    if (i === temp.length - 2) detailedRanking += " and ";
  }
  return detailedRanking;
}

module.exports.ReviewDialog = ReviewDialog;
