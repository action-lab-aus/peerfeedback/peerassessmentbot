# Peer Assessment Bot

Peer Assessment Bot is a system developed to facilitate the peer feedback process with a task-oriented chatbot. The Peer Assessment Bot offers the opportunity for students at Monash to provide and receive anonymous feedback on the draft videos that they produced for **FIT4005/FIT5125/FIT5143 Research Methods in Information Technology**, via a chat interface. Throughout the peer-assessment process, students will have the chance to take a look at other students’ works-in-progress, and get an understanding of the potential improvements that they can make for the final submission.

Peer Assessment Bot has been created using [Bot Framework](https://dev.botframework.com), which:

- Uses [LUIS](https://www.luis.ai) to implement core AI capabilities
- Implements a multi-turn conversation using Dialogs
- Prompts for and validate requests for information from the user
- Demonstrates how to handle any unexpected errors

Multiple channels (e.g. Facebook, Line, Web Chat) can be connected directly with the Peer Assessment Bot. While connecting other social media channels that are not currently supported by default, the Direct Line service needs to be configured. For more information, please visit [Direct Line API](https://docs.microsoft.com/en-us/azure/bot-service/rest-api/bot-framework-rest-direct-line-3-0-concepts?view=azure-bot-service-4.0).

## Prerequisites

### 1. [Node.js](https://nodejs.org) version 10.14 or higher

```bash
# Determine node version
node --version
```

### 2. [LUIS App](https://docs.microsoft.com/en-us/azure/cognitive-services/luis/get-started-portal-build-app)

- Create a LUIS app in [LUIS Portal](https://www.luis.ai)
- Deploy the app in [Azure Portal](https://portal.azure.com/#create/Microsoft.CognitiveServicesLUISAllInOne) with your subscription details
- Record the LUIS API Endpoint Name, API Key, and App ID for setting the environment variables

### 3. [Microsoft Computer Vision](https://azure.microsoft.com/en-us/services/cognitive-services/computer-vision/)

- Microsoft Computer Vision is used to extract the ID of the participants from the Monash ID Card that they submitted to the bot. More details can be found under `./utils/ocrHelper.js`
- Record the Computer Vision Subscription Key and Endpoint for setting the environment variables

### 4. [Firebase Project](https://console.firebase.google.com/u/0/)

- Create a Firebase project in the online console
- Download Firebase Admin Service Account
  - In the Firebase console, open Settings > Service Accounts
  - Click Generate New Private Key, then confirm by clicking Generate Key
  - Securely store the JSON file containing the key
  - Update your Service Account under `./firebase/admin.js`
- Enable Firestore and Storage
- Record the Firestore Url and Storage Url for setting the environment variables

### 5. [AWS Media Convert](https://ap-southeast-2.console.aws.amazon.com/mediaconvert/home?region=ap-southeast-2#/welcome)

AWS Elemental Media Convert is used for video transcoding, which ensures that videos can be shared within WeChat (which has a video size limitation of 20 MB). Since the videos being submitted will be around 90 seconds, setting the output video bit rate to be 500,000 can guarantee that the size will be around 7 MB (120 Seconds \* 500000 Bps = 7.3 MB).

For more details, please check `./transcode/mediaConvertParam.js` for the AWS Media Convert parameters, and `./transcode/transcoder.js` for detailed implementations.

### 6. [AWS S3 Bucket](https://docs.aws.amazon.com/s3/index.html)

AWS S3 Bucket will be used to store and retrieve original and compressed videos for Peer Assessment with high scalability and rapid access. When taking HTTP as input file sources, we need to ensure that:

- All input files must be publicly readable
- The HTTP(S) server must not require authentication
- The HTTP(S) server must accept both HEAD and range GET requests
- The URL that you specify must be a direct link to your file since MediaConvert doesn't follow redirects
- The URL that you specify can't include parameters

In that regard, some videos need to be uploaded to S3 before creating a transcoding job using Media Convert. Please check the `downloadVideo` method under `./transcode/transcoder.js` for more details.

### 7. [Amazon CloudWatch](https://aws.amazon.com/cloudwatch/)

Amazon CloudWatch is used to monitor the Media Convert Job Status. CloudWatch collects monitoring and operational data from Media Convert Events (`MediaConvertJobCompleteAlert` & `MediaConvertJobErrorAlert`), and send notification using Amazon SNS.

Since video transcoding is a long-running process that will take more than 15 seconds, the bot needs to make a response before the timeout and use the notification to remind the user of the transcoding outcome.

### 8. [Amazon Simple Notification Service](https://aws.amazon.com/sns/?whats-new-cards.sort-by=item.additionalFields.postDateTime&whats-new-cards.sort-order=desc)

Amazon Simple Notification Service is used to send `MediaConvertJobCompleteAlert` and `MediaConvertJobErrorAlert` to notify the user of the transcoding outcome. The subscription endpoint should be set as `https://yourbotendpoint/api/aws-sns`.

When creating the subscription, please copy the methods from `./backup/awsSnsConfirmation.js` into `./index.js` (and replace the `/api/aws-sns` endpoint) to process the confirmation message. After successfully creating the subscription, revert the code back.

# Run the Bot Locally

- Download the bot code from the Build blade in the Azure Portal (make sure you click "Yes" when asked "Include app settings in the downloaded zip file?")

  - If you clicked "No" you will need to copy all the Application Settings properties from your App Service to your local .env file

- Install modules

  ```
  npm install
  ```

- Run the bot

  ```
  npm start
  ```

# Testing the Not using Bot Framework Emulator

[Bot Framework Emulator](https://github.com/microsoft/botframework-emulator) is a desktop application that allows bot developers to test and debug their bots on localhost or running remotely through a tunnel.

- Install the Bot Framework Emulator version 4.5.2 or greater from [here](https://github.com/Microsoft/BotFramework-Emulator/releases)

## Connect to the bot using Bot Framework Emulator

- Launch Bot Framework Emulator
- File -> Open Bot
- Enter a Bot URL of `http://localhost:80/api/messages`

# Deploy the Bot to Azure

After creating the bot and testing it locally, you can deploy it to Azure to make it accessible from anywhere.
To learn how, see [Deploy your bot to Azure](https://aka.ms/azuredeployment) for a complete set of deployment instructions.
