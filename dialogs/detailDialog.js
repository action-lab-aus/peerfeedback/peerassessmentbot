const {
  ComponentDialog,
  TextPrompt,
  WaterfallDialog,
} = require("botbuilder-dialogs");

const {
  getUserProfile,
  getRankDetails,
  updateReplyFlag,
  getConversationRef,
  updateRankDetail,
} = require("../firebase/firebaseHelper");
const { sendVideo } = require("../utils/videoHelper");
const { checkLuisConfirm } = require("../luis/luisHelper");
const { getChannel } = require("../utils/localFileHelper");
const { sendDetailedReview } = require("../utils/apiNotify");

class DetailDialog extends ComponentDialog {
  constructor() {
    super("DetailDialog");

    // Define the main dialog and its related components.
    this.addDialog(new TextPrompt("TextPrompt"));

    this.addDialog(
      new WaterfallDialog("MainWaterfallDialog", [
        this.askDetailStep.bind(this),
        this.sendDetailStep.bind(this),
      ])
    );

    this.initialDialogId = "MainWaterfallDialog";
  }

  /**
   * Prompts the user to choose the ranking feedback for review.
   * @param {WaterfallStepContext} stepContext
   */
  async askDetailStep(stepContext) {
    // Check confirm with luis `Communication.Confirm` intent.
    if (!stepContext.options.retry) {
      if (stepContext.options.next) return await stepContext.endDialog();
      const confirm = await checkLuisConfirm(stepContext.context);
      if (!confirm) {
        await stepContext.context.sendActivity(
          "/:strong when you are able to provide more detailed comments."
        );
        return await stepContext.endDialog();
      }
    }

    const id = stepContext.options.id;
    stepContext.values.id = id;
    const role = stepContext.options.role;
    stepContext.values.role = role;
    const { name } = await getUserProfile(id, role);
    stepContext.values.name = name;

    // Get the rank details for the current user.
    var details = [];
    if (!stepContext.options.details) {
      const { rankDetails } = await getRankDetails(id, role);
      rankDetails.forEach((doc) => {
        if (!doc.data().Detail_Message) {
          details.push({ id: doc.id, data: doc.data() });
        }
      });
    } else details = stepContext.options.details;

    stepContext.values.details = details;
    const detail = details.splice(0, 1)[0];
    stepContext.values.detail = detail;
    const ranking = getRankingReference(detail.data.Rank_Provided);
    stepContext.values.ranking = ranking;

    await updateReplyFlag(id, role, false);
    const message = `Great! I'll send you 2 videos as a reminder. You ranked video 1 as ${ranking} and video 2 as the 1st for ${detail.data.Criteria}. Could you tell me why video 1 received a lower rank?`;
    await stepContext.context.sendActivity(message);
    await sendVideo(stepContext.context, `${detail.data.Student_Id}.mp4`);
    await sendVideo(
      stepContext.context,
      `${detail.data.Ranking_List["1"]}.mp4`
    );
    // Only set the reply flag back to true if the channel is not directline.
    const channel = getChannel(stepContext.context);
    if (channel !== "DIRECTLINE") await updateReplyFlag(id, role, true);

    return await stepContext.prompt("TextPrompt");
  }

  /**
   * Prompts the user for their opinion on the ranking details.
   * @param {WaterfallStepContext} stepContext
   */
  async sendDetailStep(stepContext) {
    // Update the ranking details node in Firestore.
    const msg = stepContext.result;
    const documentId = stepContext.values.detail.id;
    const studentId = stepContext.values.detail.data.Student_Id;
    const rankProvider = stepContext.values.id;
    const role = stepContext.values.role;
    const ranking = stepContext.values.ranking;
    const details = stepContext.values.details;
    const detail = stepContext.values.detail;
    await updateRankDetail(documentId, rankProvider, role, msg);

    // Send detailed reviews to the corresponding student.
    const { channel, userId } = await getUserProfile(studentId, "Student");
    const conversationRef = await getConversationRef(channel, userId);
    const message = `You have received detailed feedback about your video ranking ${ranking} for ${detail.data.Criteria}: "${msg}"`;
    await sendDetailedReview(channel, userId, conversationRef, message);

    // Send the message to the ranking provider.
    var finalMsg =
      "Perfect! I’ll pass these comments to the creator of that video!";
    if (details.length === 0) {
      await stepContext.context.sendActivity(finalMsg);
      return await stepContext.endDialog();
    }

    finalMsg +=
      " /:strong again when you are ready to respond to more requests for detail.";
    await stepContext.context.sendActivity(finalMsg);
    return await stepContext.replaceDialog(this.initialDialogId, {
      next: true,
      id: stepContext.values.id,
      role: stepContext.values.role,
      details: stepContext.values.details,
    });
  }
}

/**
 * Get the ranking reference character (1st, 2nd, 3rd or 4th).
 * @param {Integer} ranking
 */
function getRankingReference(ranking) {
  switch (ranking) {
    case "1":
      return "1st";
    case "2":
      return "2nd";
    case "3":
      return "3rd";
    case "4":
      return "4th";
  }
}

module.exports.DetailDialog = DetailDialog;
