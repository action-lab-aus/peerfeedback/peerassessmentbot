class Feedback {
  /**
   * @param {String} videoOwner
   * @param {String} shortText
   * @param {Int} audienceRating
   * @param {Int} visualRating
   * @param {String} freeText
   */
  constructor(videoOwner, shortText, audienceRating, visualRating, freeText) {
    this.videoOwner = videoOwner;
    this.shortText = shortText;
    this.audienceRating = audienceRating;
    this.visualRating = visualRating;
    this.freeText = freeText;
  }
}

module.exports.Feedback = Feedback;
