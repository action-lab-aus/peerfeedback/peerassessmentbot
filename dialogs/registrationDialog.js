const {
  ComponentDialog,
  ConfirmPrompt,
  AttachmentPrompt,
  WaterfallDialog,
} = require("botbuilder-dialogs");

const { getOcrResponse } = require("../utils/ocrHelper");
const {
  postUser,
  postWebChatUser,
  getAllUserId,
  postIdleDetection,
  deleteIdleDetection,
} = require("../firebase/firebaseHelper");
const {
  getAttachment,
  getChannel,
  getUserDetails,
} = require("../utils/localFileHelper");

class RegistrationDialog extends ComponentDialog {
  constructor() {
    super("RegistrationDialog");

    // Define the main dialog and its related components.
    this.addDialog(new ConfirmPrompt("ConfirmPrompt"));
    this.addDialog(
      new AttachmentPrompt("AttachmentPrompt", this.imagePromptValidator)
    );

    this.addDialog(
      new WaterfallDialog("IdDialog", [
        this.askNameStep.bind(this),
        this.askIdStep.bind(this),
        this.confirmIdStep.bind(this),
        this.finalIdStep.bind(this),
      ])
    );

    this.initialDialogId = "IdDialog";
  }

  /**
   * Prompts the user for the name.
   * @param {WaterfallStepContext} stepContext
   */
  async askNameStep(stepContext) {
    const id = stepContext.options.id;
    if (id) {
      await stepContext.context.sendActivity(
        `You have already registered 😝 We will let you know when there is something to do.`
      );
      return await stepContext.endDialog();
    }

    // Retrieve the student preferred name from the current activity.
    const name = stepContext.context.activity.text;
    stepContext.values.name = name;

    return await stepContext.next();
  }

  /**
   * Prompts the user for the monash id card/id number.
   * @param {WaterfallStepContext} stepContext
   */
  async askIdStep(stepContext) {
    var promptOptions = {
      retryPrompt:
        "Sorry, I could not extract a valid Monash ID 😵 Could you please send me your ID again?",
    };

    if (stepContext.options.reprompt) {
      if (stepContext.options.notClear)
        promptOptions.prompt =
          "I couldn't work out your Monash ID 😵 Try uploading another photo or entering your ID.";
      if (stepContext.options.notValid)
        promptOptions.prompt =
          "Sorry, we can't find which workshop you are in. Please contact your tutor to fix the issue 😵";
      if (stepContext.options.notConfirm)
        promptOptions.prompt =
          "Ok, could your please share a photo of your Monash Id card with me or send your ID number again?";
      if (stepContext.options.duplicate)
        promptOptions.prompt =
          "Sorry, it seems that this WeChat number is already associated with another student.";
      stepContext.values.name = stepContext.options.name;
    } else {
      await stepContext.context.sendActivity(
        `Great, thanks ${
          stepContext.values.name
        }! Could you please share a photo of your Monash ID card or enter your ID number so I can check which workshop you’re in. You can ask me for help at any time by messaging \`help\`.`
      );
    }

    return await stepContext.prompt("AttachmentPrompt", promptOptions);
  }

  /**
   * Confirm the user with the monash id extracted by ocr.
   * @param {WaterfallStepContext} stepContext
   */
  async confirmIdStep(stepContext) {
    let id;
    // If receiving a text, then set it to be the monash id.
    const tmpId = stepContext.context.activity.text;
    if (tmpId) id = tmpId;
    // Otherwise, extract the id from the image submitted.
    else {
      const imageUrl = stepContext.result[0].contentUrl;
      const userId = stepContext.context.activity.from.id;

      // Download the attachment as local file with the image url and user id as the name.
      const localFileName = await getAttachment(imageUrl, userId, "jpg");

      // Get the OCR response for extracting the monash id from the id card.
      const response = await getOcrResponse(localFileName);

      // If error occurred during the image processing, return error message.
      if (!Array.isArray(response) || !response) {
        return await stepContext.replaceDialog(this.initialDialogId, {
          reprompt: true,
          notClear: true,
          name: stepContext.values.name,
        });
      }
      id = response[0];
    }
    stepContext.values.id = id;

    // Get the user's email address and role from the enrolment list.
    var { email, role, name } = getUserDetails(id);
    if (!email)
      return await stepContext.replaceDialog(this.initialDialogId, {
        reprompt: true,
        notValid: true,
        name: stepContext.values.name,
      });

    const userIdList = await getAllUserId();
    const channel = getChannel(stepContext.context);
    if (
      userIdList.indexOf(id) >= 0 &&
      channel !== "EMULATOR" &&
      channel !== "WEBCHAT"
    )
      return await stepContext.replaceDialog(this.initialDialogId, {
        reprompt: true,
        duplicate: true,
        name: stepContext.values.name,
      });

    stepContext.values.email = email;
    stepContext.values.role = role;
    if (!stepContext.values.name) stepContext.values.name = name;

    await postIdleDetection(
      channel,
      stepContext.context.activity.from.id,
      "It's been a while since you replied. Could you confirm your Monash ID and email with YES or NO from my last message?"
    );

    return await stepContext.prompt("ConfirmPrompt", {
      prompt: `To confirm, your Monash ID number is \`${id}\` and email is \`${email}\`. Is that correct? Confirm with YES or NO.`,
    });
  }

  /**
   * If confirmed, record user information; otherwise, restart the user id dialog.
   * @param {WaterfallStepContext} stepContext
   */
  async finalIdStep(stepContext) {
    const userId = stepContext.context.activity.from.id;
    await deleteIdleDetection(userId);

    if (!stepContext.result)
      return await stepContext.replaceDialog(this.initialDialogId, {
        reprompt: true,
        notConfirm: true,
        name: stepContext.values.name,
      });

    // Retrieve the monash id, user id & channel from the stepContext.
    const id = stepContext.values.id;
    const name = stepContext.values.name;
    const email = stepContext.values.email;
    const role = stepContext.values.role;
    const channel = getChannel(stepContext.context);

    // Register the user and record the information in Firestore.
    if (channel === "WEBCHAT" || channel === "EMULATOR")
      await postWebChatUser(id, name, channel, userId, email, role);
    else await postUser(id, name, channel, userId, email, role);

    await stepContext.context.sendActivity(
      `Thanks ${name}! That’s the end of the boring admin bit! Hang tight for your first feedback task...`
    );

    return await stepContext.endDialog();
  }

  /**
   * Attachment Prompt Validator to check whether the attachment is an image.
   * @param {PromptValidatorContext} promptContext
   */
  async imagePromptValidator(promptContext) {
    if (!promptContext.recognized.succeeded) {
      try {
        // If not an image attachment, then try checking the text.
        // If it matches a valid Monash ID, return true as well.
        const str = promptContext.context.activity.text;
        if (str.match(/[0-9]{8}/g)) return true;
      } catch (error) {
        return false;
      }
      return false;
    }
    const contentType = promptContext.recognized.value[0].contentType;
    return contentType.startsWith("image") ? true : false;
  }
}

exports.RegistrationDialog = RegistrationDialog;
