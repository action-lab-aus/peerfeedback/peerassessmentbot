const {
  ComponentDialog,
  TextPrompt,
  ConfirmPrompt,
  NumberPrompt,
  WaterfallDialog,
} = require("botbuilder-dialogs");

const { firebase } = require("../../firebase/admin");
const {
  getStudentName,
  postFeedback,
} = require("../../firebase/firebaseHelper");
const { Feedback } = require("../../models/archive/feedback");
const { getVideoOwner, sendVideo } = require("../../utils/videoHelper");
const CONSTANTS = require("../../constants");

// Create Firebase reference to the user node.
const userProfileRef = firebase.collection(
  `${CONSTANTS.ASSESS_PHASE}/User/UserProfile`
);

class FeedbackDialog extends ComponentDialog {
  constructor() {
    super("FeedbackDialog");

    // Define the main dialog and its related components.
    this.addDialog(new TextPrompt("TextPrompt"));
    this.addDialog(new TextPrompt("ShortTextPrompt", this.textPromptValidator));
    this.addDialog(new ConfirmPrompt("ConfirmPrompt"));
    this.addDialog(
      new NumberPrompt("NumberPrompt", this.ratingPromptValidator)
    );

    this.addDialog(
      new WaterfallDialog("MainWaterfallDialog", [
        this.shortTextStep.bind(this),
        this.audienceRatingStep.bind(this),
        this.visualRatingStep.bind(this),
        this.freeTextStep.bind(this),
        this.confirmStep.bind(this),
        this.summaryStep.bind(this),
      ])
    );

    this.initialDialogId = "MainWaterfallDialog";
  }

  // Prompts the user for the short text comment.
  async shortTextStep(stepContext) {
    const studentId = stepContext.options.studentId;
    const ignoreLimit = stepContext.options.ignoreLimit;
    const retry = stepContext.options.retry;
    stepContext.values.studentId = studentId;
    stepContext.values.ignoreLimit = ignoreLimit;

    const studentName = await getStudentName(studentId);
    stepContext.values.studentName = studentName;

    // Get number of feedback of the current student.
    const snapshot = await userProfileRef.doc(`${studentId}`).get();
    const feedbackNo = snapshot.data().Provided_No;

    if (feedbackNo >= CONSTANTS.MAX_FEEDBACK_NO && ignoreLimit !== true) {
      await stepContext.context.sendActivity(
        `You have already provided all the feedback required 😝\n${CONSTANTS.BROADCAST_MESSAGE.Help}`
      );
      return await stepContext.endDialog();
    }

    var videoOwner;
    // If it is a replaced prompt, set the videoOwner information from the stepContext.
    if (stepContext.options.videoOwner !== undefined) {
      videoOwner = stepContext.options.videoOwner;

      // If ignoreLimit, then send the video to the user first
      if (ignoreLimit && !retry) {
        userProfileRef.doc(studentId).update({ Reply_Flag: false });
        await sendVideo(stepContext.context, `${videoOwner}.mp4`);
        userProfileRef.doc(studentId).update({ Reply_Flag: true });
      }
    } else {
      // Get the current number of feedback provided.
      const indexSnapshot = await userProfileRef.doc(`${studentId}`).get();
      const index = indexSnapshot.data().Provided_No;

      // Assign the video to be assessed.
      videoOwner = await getVideoOwner(studentId, index);
      userProfileRef.doc(studentId).update({ Reply_Flag: false });

      await stepContext.context.sendActivity(
        `Hi, ${stepContext.values.studentName}. You have provided ${index} out of ${CONSTANTS.MAX_FEEDBACK_NO} feedback. I will retrieve one more video for you to review, please wait for a moment 🕙`
      );

      // Get the video from Firebase and send to the user.
      await sendVideo(stepContext.context, `${videoOwner}.mp4`);

      userProfileRef.doc(studentId).update({ Reply_Flag: true });
    }

    const feedback = new Feedback(videoOwner);
    stepContext.values.feedback = feedback;

    if (retry)
      await stepContext.context.sendActivity(
        "Sure, please provide the feedback again."
      );

    const promptOptions = {
      prompt: "In less than 5 words, describe what this video is about?",
      retryPrompt:
        "Oops, the descrtiption should be less than 5 words 😵 Could you provide it again?",
    };

    return await stepContext.prompt("ShortTextPrompt", promptOptions);
  }

  // Prompts the user for the communicating with audience rating.
  async audienceRatingStep(stepContext) {
    stepContext.values.feedback.shortText = stepContext.result;

    const promptOptions = {
      prompt:
        "How clear is this video at communicating to the audience? Rate it between 1 and 5.",
      retryPrompt:
        "Oops, the rating should be a valid number between 1 and 5 😵 Could you provide the rating again?",
    };

    return await stepContext.prompt("NumberPrompt", promptOptions);
  }

  // Prompts the user for the visual element usage rating.
  async visualRatingStep(stepContext) {
    stepContext.values.feedback.audienceRating = stepContext.result;

    const promptOptions = {
      prompt:
        "How appropriate were the visual elements that were used in this video? Rate it between 1 and 5.",
      retryPrompt:
        "Oops, the rating should be a valid number between 1 and 5 😵 Could you provide the rating again?",
    };

    return await stepContext.prompt("NumberPrompt", promptOptions);
  }

  // Prompts the user for the free text comment.
  async freeTextStep(stepContext) {
    stepContext.values.feedback.visualRating = stepContext.result;

    return await stepContext.prompt(
      "TextPrompt",
      "Suggest one thing that you would change to improve this video?"
    );
  }

  // Confirm the user with the feedback object provided.
  async confirmStep(stepContext) {
    stepContext.values.feedback.freeText = stepContext.result;
    const feedback = stepContext.values.feedback;

    // Define the feedback message for the student to confirm
    const feedbackMsg = `
    The feedback you have provided is:\n
    Q1. In less than 5 words, describe what this video is about: ${feedback.shortText}\n 
    Q2. How clear is this video at communicating to the audience: ${feedback.audienceRating}\n 
    Q3. How appropriate were the visual elements that were used in this video: ${feedback.visualRating}\n 
    Q4. Suggest one thing that you would change to improve this video: ${feedback.freeText}\n
    Please confirm with yes if it is correct.`;

    return await stepContext.prompt("ConfirmPrompt", { prompt: feedbackMsg });
  }

  // Final step for summarzie the dialog.
  async summaryStep(stepContext) {
    // If not confirmed, restart the comment dialog.
    if (!stepContext.result)
      return await stepContext.replaceDialog(this.initialDialogId, {
        videoOwner: stepContext.values.feedback.videoOwner,
        studentId: stepContext.values.studentId,
        ignoreLimit: stepContext.values.ignoreLimit,
        retry: true,
      });

    // Post feedback into Firebase.
    await postFeedback(
      stepContext.values.feedback,
      stepContext.values.studentId
    );

    var message;
    if (stepContext.values.ignoreLimit) {
      message = `Thanks for all your hard work, ${stepContext.values.studentName}. We have a couple more videos that we want your feedback on. If you are interested, try to reply "Y" again to review more videos!`;
    } else {
      // Retrieve the number of feedback provided.
      const currentUserRef = userProfileRef.doc(stepContext.values.studentId);
      const providedSnapshot = await currentUserRef.get();
      const providedNo = providedSnapshot.data().Provided_No;

      if (providedNo < CONSTANTS.MAX_FEEDBACK_NO)
        message = `Thanks for all your hard work, ${stepContext.values.studentName}. We have a couple more videos that we want your feedback on. Chat with me again whenever you are available!`;
      else
        message = `You’re all done, ${stepContext.values.studentName}! I’ll let you know when feedback comes in for your video...`;
    }

    await stepContext.context.sendActivity(message);
    return await stepContext.endDialog();
  }

  // Text Prompt Validator to check whether the short text is less than 5 words.
  async textPromptValidator(promptContext) {
    return (
      promptContext.recognized.succeeded &&
      promptContext.recognized.value.split(" ").length <= 5
    );
  }

  // Number Prompt Validator to check whether the rating is a valid integer within 1 to 5.
  async ratingPromptValidator(promptContext) {
    if (
      promptContext.recognized.succeeded &&
      !isNaN(promptContext.recognized.value)
    ) {
      const rating = parseInt(promptContext.recognized.value);
      return rating >= 1 && rating <= 5;
    }
    return false;
  }
}

module.exports.FeedbackDialog = FeedbackDialog;
