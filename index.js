// Import required bot services.
const {
  BotFrameworkAdapter,
  ConversationState,
  InputHints,
  UserState,
} = require("botbuilder");

// Import bot services and logger for log middleware.
const { TranscriptLoggerMiddleware } = require("botbuilder-core");
const { CustomLogger } = require("./logger/CustomLogger");

// Import required packages.
const request = require("request");
const bodyParser = require("body-parser");
const restify = require("restify");
const { firebase } = require("./firebase/admin");
const { getChannel } = require("./utils/localFileHelper");
const { downloadS3Resource } = require("./transcode/transcoder");
const { sendImage } = require("./utils/videoHelper");
const { sendEmail } = require("./utils/mailHelper");
const { checkLuisHelp, getHelpMessage } = require("./luis/luisHelper");
const {
  initializeDb,
  getMonashId,
  getUserProfile,
  getConversationRef,
  getAllConversations,
  postStudentIndex,
  postAssessStatus,
  updateReplyFlag,
} = require("./firebase/firebaseHelper");
const CONSTANTS = require("./constants");

const path = require("path");
const dotenv = require("dotenv");
const ENV_FILE = path.join(__dirname, ".env");
dotenv.config({ path: ENV_FILE });

// Configure the connection settings to connect to Cosmos DB.
const { CosmosDbStorage } = require("botbuilder-azure");
const cosmosStorage = new CosmosDbStorage({
  serviceEndpoint: process.env.COSMOS_DB_SERVICE_ENDPOINT,
  authKey: process.env.COSMOS_AUTH_KEY,
  databaseId: process.env.COSMOS_DATABASE,
  collectionId: process.env.COSMOS_CONTAINER,
});

// Define the bot's main dialog.
const { DialogBot } = require("./bots/dialogBot");
const { MainDialog } = require("./dialogs/mainDialog");

// Create Firebase reference to the setting node.
const settingRef = firebase.doc(`${process.env.ASSESS_PHASE}/Setting`);
const wechatSettingRef = firebase.doc(
  `${process.env.ASSESS_PHASE}/WeChatSetting`
);

// Configure middleware.
const logstore = new CustomLogger();
const logActivity = new TranscriptLoggerMiddleware(logstore);

// Create bot framework adapter.
// Use the transcript logger middleware to log all incoming and outgoing messages.
const adapter = new BotFrameworkAdapter({
  appId: process.env.MicrosoftAppId,
  appPassword: process.env.MicrosoftAppPassword,
}).use(logActivity);

// Initialize the bot connector and trust service urls for proactive messages.
const { MicrosoftAppCredentials } = require("botframework-connector");

// Catch-all for errors.
adapter.onTurnError = async (context, error) => {
  // This check writes out errors to console log.
  console.error(`\n [onTurnError]: ${error}`);

  // Send error message to the user.
  const onTurnErrorMessage = error;

  await context.sendActivity(
    onTurnErrorMessage,
    onTurnErrorMessage,
    InputHints.ExpectingInput
  );
  // Clear out state.
  await conversationState.delete(context);
};

// Define a state store for your bot.
// A bot requires a state store to persist the dialog and user state between messages.
let conversationState, userState;

// Using Cosmos DB for storing conversation and user state.
conversationState = new ConversationState(cosmosStorage);
userState = new UserState(cosmosStorage);

// Create HTTP server.
const server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3980, function() {
  console.log(`\n${server.name} listening to ${server.url}`);
});

const conversationReferences = {};
// Create the main dialog.
const mainDialog = new MainDialog(userState);
const bot = new DialogBot(
  conversationState,
  userState,
  mainDialog,
  conversationReferences
);

// Listen for incoming activities and route them to the bot registration dialog.
server.post("/api/messages", (req, res) => {
  // Route received a request to adapter for processing.
  adapter.processActivity(req, res, async (turnContext) => {
    // Retrieve the user id and channel from the current turnContext.
    const userId = turnContext.activity.from.id;
    const channel = getChannel(turnContext);
    // Retrieve the student id from Firebase.
    var { id, role } = await getMonashId(channel, userId);

    // Send warning message to the user when the video is being processed.
    if (id) {
      const { replyFlag } = await getUserProfile(id, role);
      if (replyFlag === false)
        return await turnContext.sendActivity(
          "I am still processing the video. Please wait for a couple more minutes."
        );
    }

    // Check whether help intent has been detected.
    if (turnContext.activity.type === "message" && turnContext.activity.text) {
      const help = await checkLuisHelp(turnContext);
      if (help) {
        const message = await getHelpMessage(id, role);
        return await turnContext.sendActivity(message);
      }
    }

    return await bot.run(turnContext);
  });
});

// Listen for incoming notifications and send proactive messages to users
// for detailed video comments.
server.post(
  "/api/notify/review",
  bodyParser.urlencoded({
    extended: true,
  }),
  bodyParser.json(),
  async (req, res) => {
    console.log("Notification Request Body: ", req.body);
    const message = req.body.message;
    const userId = req.body.userId;
    const channel = req.body.channel;

    // Request body verification.
    if (!message || !userId || !channel) {
      console.log("Paramter Invalid...");
      res.writeHead(400);
      return res.end();
    }

    await sendEmail(userId, channel, message);

    res.writeHead(200);
    res.end();
  }
);

server.post(
  "/api/aws-sns",
  bodyParser.urlencoded({
    extended: true,
  }),
  bodyParser.text(),
  async (req, res) => {
    try {
      const payload = JSON.parse(req.body);
      const message = JSON.parse(payload.Message);
      if (message.detail.status === "COMPLETE") {
        const outputGroup = message.detail.outputGroupDetails[0].outputDetails;
        downloadS3Resource(outputGroup);
        res.writeHead(200);
      } else res.writeHead(400);
      res.end();
    } catch (error) {
      console.error(error);
      res.writeHead(400);
      res.end();
    }
  }
);

// Listen for incoming notifications and send proactive messages to users
// for video submission confirmation.
server.post(
  "/api/notify",
  bodyParser.urlencoded({
    extended: true,
  }),
  bodyParser.json(),
  async (req, res) => {
    console.log("Notification Request Body: ", req.body);
    const channel = req.body.channel;
    const userId = req.body.userId;
    const message = req.body.message;
    const videoName = req.body.videoName;
    // Request body verification.
    if (!channel || !userId || !message) {
      console.log("Paramter Invalid...");
      res.writeHead(400);
      return res.end();
    }

    const conversationRef = await getConversationRef(channel, userId);
    try {
      await adapter.continueConversation(
        conversationRef,
        async (turnContext) => {
          MicrosoftAppCredentials.trustServiceUrl(conversationRef.serviceUrl);
          // If not successful, send back the message.
          if (!videoName) return await turnContext.sendActivity(message);
          else {
            // If successful, send the thumbnail to confirm the video submitted.
            const { id, role } = await getMonashId(channel, userId);
            await updateReplyFlag(id, role, true);
            await turnContext.sendActivity(message);
            return await sendImage(turnContext, videoName);
          }
        }
      );
    } catch (error) {
      console.error(error);
    }

    res.writeHead(200);
    res.end();
  }
);

// Register real-time listener for the setting modification in Firebase.
settingRef.onSnapshot(
  async (docSnapshot) => {
    // Retrieve the previous setting information.
    if (!docSnapshot.data()) return;

    var previousSetting = docSnapshot.data().Previous_Setting;
    const uploadFlag = docSnapshot.data().Upload;
    const rankingFlag = docSnapshot.data().Ranking;
    const reallocateFlag = docSnapshot.data().Reallocate;
    const reviewFlag = docSnapshot.data().Review;
    const summaryFlag = docSnapshot.data().Summary;

    // Update the previous setting & send broadcast messages accordingly.
    if (uploadFlag && uploadFlag !== previousSetting.Upload) {
      previousSetting.Upload = uploadFlag;
      wechatSettingRef.update({ Upload: uploadFlag });
      settingRef.update({ Previous_Setting: previousSetting });
      await broadcastMessage("Upload");
    }
    if (rankingFlag && rankingFlag !== previousSetting.Ranking) {
      previousSetting.Ranking = rankingFlag;
      wechatSettingRef.update({ Ranking: rankingFlag });
      settingRef.update({ Previous_Setting: previousSetting });
      await broadcastMessage("Ranking");
    }
    if (reallocateFlag && reallocateFlag !== previousSetting.Reallocate) {
      previousSetting.Reallocate = reallocateFlag;
      wechatSettingRef.update({ Reallocate: reallocateFlag });
      settingRef.update({ Previous_Setting: previousSetting });
      await broadcastMessage("Reallocate");
    }
    if (reviewFlag && reviewFlag !== previousSetting.Review) {
      previousSetting.Review = reviewFlag;
      wechatSettingRef.update({ Review: reviewFlag });
      settingRef.update({ Previous_Setting: previousSetting });
      await broadcastMessage("Review");
    }
    if (summaryFlag && summaryFlag !== previousSetting.Summary) {
      previousSetting.Summary = summaryFlag;
      wechatSettingRef.update({ Summary: summaryFlag });
      settingRef.update({ Previous_Setting: previousSetting });
      await broadcastMessage("Summary");
    }
  },
  (error) => {
    console.error("Encountered error: ", error);
  }
);

// Send broadcast messages to all registered students.
async function broadcastMessage(stage) {
  // Retrieve all conversation references from Firebase.
  const allConversationRefs = await getAllConversations();
  if (stage === "Ranking") await postStudentIndex();
  if (stage === "Reallocate") await postAssessStatus();

  for (const ref of Object.values(allConversationRefs)) {
    conversationRef = JSON.parse(ref);
    await adapter.continueConversation(conversationRef, async (turnContext) => {
      MicrosoftAppCredentials.trustServiceUrl(conversationRef.serviceUrl);

      try {
        await turnContext.sendActivity(CONSTANTS.BROADCAST_MESSAGE[stage]);
      } catch (error) {
        console.error("Broadcasting Error: ", error);
      }
    });
  }
}
