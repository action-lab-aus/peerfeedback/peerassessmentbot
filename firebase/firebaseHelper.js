const path = require("path");
const { admin, firebase, bucket } = require("../firebase/admin");
const CONSTANTS = require("../constants");
const { ASSESS_PHASE, MAX_FEEDBACK_NO } = process.env;

const userRef = firebase.doc(`${ASSESS_PHASE}/User`);
const channelRef = firebase.doc(`${ASSESS_PHASE}/Channel`);
const userProfileRef = firebase.collection(`${ASSESS_PHASE}/User/UserProfile`);
const tutorRef = firebase.collection(`${ASSESS_PHASE}/User/TutorProfile`);
const idleRef = firebase.collection(`${ASSESS_PHASE}/Idle/Messages`);
const assessStatusRef = firebase.collection(
  `${ASSESS_PHASE}/User/AssessStatus`
);
const settingRef = firebase.doc(`${ASSESS_PHASE}/Setting`);
const wechatSettingRef = firebase.doc(`${ASSESS_PHASE}/WeChatSetting`);

/* ############################## Firestore GET Methods ############################## */

/**
 * Get the monash id from Firestore.
 * @param {String} channel The channel used by the monash.
 * @param {String} userId The user id from the current channel.
 */
async function getMonashId(channel, userId) {
  try {
    const snapshot = await channelRef
      .collection(channel)
      .doc(userId)
      .get();
    if (snapshot.data())
      return { id: snapshot.data().Monash_Id, role: snapshot.data().Role };
    return { id: null, role: null };
  } catch (error) {
    console.error(error);
  }
}

/**
 * Get the user profile from Firestore.
 * @param {String} id Current user's monash id number.
 * @param {String} role Current user's role.
 */
async function getUserProfile(id, role) {
  try {
    let currentRef;
    if (role === "Student") currentRef = userProfileRef.doc(id);
    else currentRef = tutorRef.doc(id);
    const snapshot = await currentRef.get();

    return {
      index: snapshot.data().Index,
      name: snapshot.data().User_Name,
      channel: snapshot.data().Channel,
      userId: snapshot.data().User_Id,
      videoUrl: snapshot.data().Video_Url,
      replyFlag: snapshot.data().Reply_Flag,
      audienceFlag: snapshot.data().Audience_Flag,
      visualFlag: snapshot.data().Visual_Flag,
      knowledgeFlag: snapshot.data().Knowledge_Flag,
      providedNo: snapshot.data().Rank_Provided_No,
      receivedNo: snapshot.data().Rank_Received_No,
      email: snapshot.data().Email,
    };
  } catch (error) {
    console.error(error);
  }
}

/**
 * Check the user document exists in Firestore.
 * @param {String} id Current user's monash id number.
 * @param {String} role Current user's role.
 */
async function getUser(id, role) {
  try {
    let currentRef;
    if (role === "Student") currentRef = userProfileRef.doc(id);
    else currentRef = tutorRef.doc(id);
    const snapshot = await currentRef.get();
    return snapshot.exists;
  } catch (error) {
    console.error(error);
  }
}

/**
 * Get all the registered user ids from Firestore.
 */
async function getAllUserId() {
  try {
    const studentSnapshot = await userProfileRef.get();
    const tutorSnapshot = await tutorRef.get();

    let userIdList = [];
    studentSnapshot.forEach((doc) => {
      userIdList.push(doc.id);
    });
    tutorSnapshot.forEach((doc) => {
      userIdList.push(doc.id);
    });
    return userIdList;
  } catch (error) {
    console.error(error);
  }
}

/**
 * Get the conversation reference for the current user.
 * @param {String} channel The channel used by the student.
 * @param {String} userId The user id from the current channel.
 */
async function getConversationRef(channel, userId) {
  try {
    const snapshot = await channelRef
      .collection(channel)
      .doc(userId)
      .get();
    const ref = snapshot.data().Conversation_Details;
    return JSON.parse(ref);
  } catch (error) {
    console.error(error);
  }
}

/**
 * Get all the user conversation details from Firestore.
 */
async function getAllConversations() {
  try {
    const facebookSnapshot = await channelRef.collection("FACEBOOK").get();
    const lineSnapshot = await channelRef.collection("LINE").get();
    var conversationReferences = [];

    facebookSnapshot.forEach((doc) => {
      const conversationId = doc.data().Conversation_Id;
      const conversationDetails = doc.data().Conversation_Details;
      if (conversationId !== undefined && conversationDetails !== undefined)
        conversationReferences[conversationId] = conversationDetails;
    });

    lineSnapshot.forEach((doc) => {
      const conversationId = doc.data().Conversation_Id;
      const conversationDetails = doc.data().Conversation_Details;
      if (conversationId !== undefined && conversationDetails !== undefined)
        conversationReferences[conversationId] = conversationDetails;
    });

    return conversationReferences;
  } catch (error) {
    console.error(error);
  }
}

/**
 * Get the student video from firebase storage, return whether the video exists or not.
 * @param {String} studentId Current user's student id number.
 */
async function getStudentVideo(studentId) {
  try {
    const file = bucket.file(`${ASSESS_PHASE}/Videos/${studentId}.mp4`);
    const videoExistence = await file.exists();
    return videoExistence[0];
  } catch (error) {
    console.error(error);
  }
}

/**
 * Get the current peer assessment stage from the setting node in Firestore.
 */
async function getCurrentStage() {
  try {
    const snapshot = await settingRef.get();
    const uploadFlag = snapshot.data().Upload;
    const rankingFlag = snapshot.data().Ranking;
    const reallocateFlag = snapshot.data().Reallocate;
    const reviewFlag = snapshot.data().Review;
    const summaryFlag = snapshot.data().Summary;

    if (summaryFlag) return "Summary";
    if (reviewFlag) return "Review";
    if (reallocateFlag) return "Reallocate";
    if (rankingFlag) return "Ranking";
    if (uploadFlag) return "Upload";
    return "Registration";
  } catch (error) {
    console.error(error);
  }
}

/**
 * Get the ranking details for the current user (no. of details required and finish flag).
 * @param {String} id Current user's monash id number.
 * @param {String} role Role of the current user.
 */
async function getRankDetails(id, role) {
  try {
    let rankDetailRef;
    if (role === "Student") rankDetailRef = userProfileRef.doc(id);
    else rankDetailRef = tutorRef.doc(id);

    const snapshot = await rankDetailRef.collection("Rank_Detail").get();

    var finished = true;
    snapshot.forEach((doc) => {
      if (!doc.data().Detail_Message) {
        finished = false;
      }
    });
    return { rankDetails: snapshot, finished: finished };
  } catch (error) {
    console.error(error);
  }
}

/**
 * Get 3 random videos to be reallocated using the shuffle function.
 * @param {String} id Current user's id number.
 */
async function getRandomVideos(studentId) {
  try {
    const snapshot = await assessStatusRef.get();
    var docIdList = [];
    snapshot.docs.forEach((doc) => {
      docIdList.push(doc.id);
    });
    // console.log("Document Id List: ", docIdList);

    const docSize = docIdList.length;
    if (docSize < 3)
      return [snapshot.docs[0], snapshot.docs[1], snapshot.docs[2]];

    var count = 0;
    var ranNums = [];
    while (count < 3) {
      ranNums = shuffle([...Array(docSize).keys()]);
      // console.log("Random Numbers: ", ranNums);
      if (
        snapshot.docs[ranNums[0]].id !== studentId &&
        snapshot.docs[ranNums[1]].id !== studentId &&
        snapshot.docs[ranNums[2]].id !== studentId
      )
        break;
      count++;
    }

    if (count === 3) return null;

    return [
      snapshot.docs[ranNums[0]],
      snapshot.docs[ranNums[1]],
      snapshot.docs[ranNums[2]],
    ];
  } catch (error) {
    console.error(error);
  }
}

/**
 * Get the current registered user number.
 */
async function getUserCount() {
  try {
    const snapshot = await userRef.get();
    return snapshot.data().UserCount;
  } catch (error) {
    console.error(error);
  }
}

/**
 * Get the current registered user list.
 */
async function getUserList() {
  try {
    const snapshot = await userRef.get();
    return snapshot.data().UserList;
  } catch (error) {
    console.error(error);
  }
}

async function getUserRole(id) {
  try {
    const snapshot = await tutorRef.get();
    let role = "Student";
    snapshot.forEach((doc) => {
      if (doc.id === id) {
        role = "Tutor";
        return true;
      }
    });
    return role;
  } catch (error) {
    console.error(error);
  }
}

/**
 * Get the student id based on the student's index number.
 * @param {Integer} index Current user's index number.
 */
async function getStudentIdByIndex(index) {
  try {
    var studentId;
    const snapshot = await userProfileRef.get();
    snapshot.forEach((doc) => {
      if (doc.data().Index === index) {
        studentId = `${doc.id}`;
        return true;
      }
    });
    return studentId;
  } catch (error) {
    console.error(error);
  }
}

/**
 * Get a signed Url of video from Firestore.
 * @param {String} videoOwner The current video owner's student id.
 */
async function getVideoUrl(videoOwner) {
  try {
    const config = {
      action: "read",
      expires: "03-17-2025",
    };
    const file = bucket.file(`${ASSESS_PHASE}/Videos/${videoOwner}`);
    const data = await file.getSignedUrl(config);
    return data[0];
  } catch (error) {
    console.error(error);
    return undefined;
  }
}

/**
 * Get a signed Url of the video thumbnail from Firestore.
 * @param {String} videoOwner The current video owner's student id.
 */
async function getThumbnailUrl(videoOwner) {
  try {
    const config = {
      action: "read",
      expires: "03-17-2025",
    };
    const file = bucket.file(
      `${ASSESS_PHASE}/Thumbnails/${videoOwner.split(".")[0]}.jpg`
    );
    const data = await file.getSignedUrl(config);
    return data[0];
  } catch (error) {
    console.error(error);
    return undefined;
  }
}

/**
 * Get the rank received list for the current student.
 * @param {String} studentId Current user's student id number.
 */
async function getRankReceived(studentId) {
  try {
    const snapshot = await userProfileRef
      .doc(studentId)
      .collection("Rank_Received")
      .get();

    var rankList = [];
    snapshot.forEach((doc) => {
      rankList.push(doc.data());
    });
    return rankList;
  } catch (error) {
    console.error(error);
  }
}

/* ############################## Firestore POST Methods ############################## */

/**
 * Post the index of each student who has submitted their videos
 * when entering the ranking stage.
 */
async function postStudentIndex() {
  try {
    let userCount = 0;
    let userList = [];
    const snapshot = await userProfileRef.get();

    const promises = [];
    snapshot.forEach(async (doc) => {
      const id = doc.id;
      if (doc.data().Submission_Time) {
        promises.push(userProfileRef.doc(id).update({ Index: ++userCount }));
        userList.push(id);
      }
    });
    promises.push(userRef.update({ UserCount: userCount }));
    await Promise.all(promises);

    // Set the userIndex array.
    await userRef.update({ UserList: userList });
    await userRef.update({ UserListBackup: userList });
  } catch (error) {
    console.error(error);
  }
}

/**
 * Post the assess status to Firestore when entering the reallocate stage.
 */
async function postAssessStatus() {
  try {
    // Retrieve all assess status from Firestore.
    const snapshot = await userProfileRef.get();
    var assessStatus = [];
    snapshot.forEach((doc) => {
      const studentId = doc.id;
      const rankReceivedNo = doc.data().Rank_Received_No;

      if (doc.data().Index) {
        if (rankReceivedNo < MAX_FEEDBACK_NO * 3)
          assessStatus.push({
            Student_Id: studentId,
            Rank_Received_No: rankReceivedNo,
          });

        if (!rankReceivedNo)
          assessStatus.push({
            Student_Id: studentId,
            Rank_Received_No: 0,
          });
      }
    });

    // Push the results to Firestore.
    for (i = 0; i < assessStatus.length; i++) {
      const assessRef = assessStatusRef.doc(assessStatus[i].Student_Id);
      await assessRef.set({
        Rank_Received_No: assessStatus[i].Rank_Received_No,
      });
    }
  } catch (error) {
    console.error(error);
  }
}

/**
 *
 * Post the user document in Firestore.
 * @param {String} id Current user id.
 * @param {String} name Current user preferred name.
 * @param {String} channel The channel used by the user.
 * @param {String} userId The user id from the current channel.
 * @param {String} email User's Monash email address.
 * @param {String} role Role of the current user.
 */
async function postUser(id, name, channel, userId, email, role) {
  try {
    // Create user node in Firebase based on the role
    if (role == "Student") {
      const currentUserRef = userProfileRef.doc(id);
      await currentUserRef.set({
        Reply_Flag: true,
        Rank_Provided_No: 0,
        Rank_Received_No: 0,
        Channel: channel,
        User_Id: userId,
        User_Name: name,
        Email: email,
        Created_At: admin.firestore.FieldValue.serverTimestamp(),
      });
    } else {
      const currentTutorRef = tutorRef.doc(id);
      await currentTutorRef.set({
        Reply_Flag: true,
        Rank_Provided_No: 0,
        Channel: channel,
        User_Id: userId,
        User_Name: name,
        Email: email,
        Created_At: admin.firestore.FieldValue.serverTimestamp(),
      });
    }
    await postChannelRef(channel, userId, id, role);
  } catch (error) {
    console.error(error);
  }
}

/**
 *
 * @param {*} channel The channel used by the user.
 * @param {*} userId The user id from the current channel.
 * @param {*} message Idle message to be sent.
 */
async function postIdleDetection(channel, userId, message) {
  try {
    await idleRef.doc(userId).set({
      Channel: channel,
      User_Id: userId,
      Message: message,
      Created_At: admin.firestore.FieldValue.serverTimestamp(),
    });
  } catch (error) {
    console.error(error);
  }
}

/**
 * // Create channel node in Firebase.
 * @param {*} channel The channel used by the user.
 * @param {*} userId The user id from the current channel.
 * @param {*} id Current user id.
 * @param {*} role Role of the current user.
 */
async function postChannelRef(channel, userId, id, role) {
  const userChannelRef = channelRef.collection(channel).doc(userId);
  await userChannelRef.update({
    Monash_Id: id,
    Role: role,
    Created_At: admin.firestore.FieldValue.serverTimestamp(),
  });
}

/**
 *
 * Post the wechat user document in Firestore.
 * @param {String} id Current user id.
 * @param {String} name Current user preferred name.
 * @param {String} channel The channel used by the user.
 * @param {String} userId The user id from the current channel.
 * @param {String} email User's Monash email address.
 * @param {String} role Role of the current user.
 */
async function postWebChatUser(id, name, channel, userId, email, role) {
  try {
    const userExists = await getUser(id, role);
    if (userExists) {
      updateUserProfile(id, role, userId, channel);
    } else {
      postUser(id, name, channel, userId, email, role);
    }
    await postChannelRef(channel, userId, id, role);
  } catch (error) {
    console.error(error);
  }
}

/**
 * Post the video submission time in Firestore.
 * @param {String} studentId Current student id.
 * @param {String} videoUrl Original url of the submitted video.
 */
async function postVideoSubmission(studentId, videoUrl) {
  try {
    const currentUserRef = userProfileRef.doc(studentId);
    await currentUserRef.update({
      Video_Url: videoUrl,
      Submission_Time: admin.firestore.FieldValue.serverTimestamp(),
    });
  } catch (error) {
    console.error(error);
  }
}

/**
 * Post the ranking retrieved from the current user.
 * @param {String} id The feedback provided user id.
 * @param {String} role Role of the current user.
 * @param {Array} videoOwnerList The list of the video owners.
 * @param {String} audienceRank  Audience communication ranking.
 * @param {String} visualRank Visual element ranking.
 * @param {String} knowledgeRank Ranking for responding to audience's knowledge.
 */
async function postRanking(
  id,
  role,
  videoOwnerList,
  audienceRank,
  visualRank,
  knowledgeRank
) {
  try {
    // Update the feedback provided for the current user.
    let providerRef;
    if (role === "Student") providerRef = userProfileRef.doc(id);
    else providerRef = tutorRef.doc(id);

    var { providedNo } = await getUserProfile(id, role);
    await providerRef.collection("Rank_Provided").add({
      Audience_Rank: audienceRank,
      Visual_Rank: visualRank,
      Knowledge_Rank: knowledgeRank,
      Created_At: admin.firestore.FieldValue.serverTimestamp(),
    });
    await providerRef.update({ Rank_Provided_No: providedNo + 1 });

    // Update the feedback received for the video owner.
    for (i = 0; i < videoOwnerList.length; i++) {
      const ownerRef = userProfileRef.doc(videoOwnerList[i]);
      const ownerRole = "Student";
      var { receivedNo } = await getUserProfile(videoOwnerList[i], ownerRole);
      await ownerRef.collection("Rank_Received").add({
        Rank_Provider: id,
        Audience_Rank: audienceRank,
        Visual_Rank: visualRank,
        Knowledge_Rank: knowledgeRank,
        Created_At: admin.firestore.FieldValue.serverTimestamp(),
      });
      await ownerRef.update({ Rank_Received_No: receivedNo + 1 });
    }
  } catch (error) {
    console.error(error);
  }
}

/**
 * Post the user conversation details to Firestore.
 * @param {String} channel The channel used by the student.
 * @param {String} userId The user id from the current channel.
 * @param {String} reference Conversation id for reference.
 * @param {*} conversation Conversation details.
 */
async function postConversation(channel, userId, reference, conversation) {
  const userChannelRef = channelRef.collection(channel).doc(userId);
  try {
    // Update the conversation reference details.
    await userChannelRef.update({
      Conversation_Id: reference,
      Conversation_Details: conversation,
    });
  } catch (error) {
    // Error indicates that the document has not been created yet, then use `set`.
    await userChannelRef.set({
      Conversation_Id: reference,
      Conversation_Details: conversation,
    });
  }
}

/**
 * Post the file to firebase storage.
 * @param {String} path Local path of the file.
 * @param {String} fileType Type of the file.
 * @param {Object} metadata Metadata of the file.
 * @param {String} fileName Name of the file.
 */
async function postFile(path, fileType, metadata, fileName) {
  try {
    await bucket.upload(path, {
      destination: `${ASSESS_PHASE}/${fileType}/${fileName}`,
      metadata: metadata,
      resumable: false,
    });
  } catch (error) {
    console.error(error);
  }
}

/**
 * Record log messages in Firestore.
 * @param {String} channel The channel used by the student.
 * @param {String} userId The user id from the current channel.
 * @param {String} timestamp The timestamp of the messages sent/received.
 * @param {String} logText The log message content.
 * @param {String} userName The user name of the message sender.
 */
function postLogMessage(channel, userId, timestamp, logText, userName) {
  try {
    const logCollectionRef = channelRef
      .collection(channel)
      .doc(userId)
      .collection("Log_Message")
      .doc(JSON.stringify(timestamp));
    logCollectionRef.set({
      Activity: logText,
      Created_At: admin.firestore.FieldValue.serverTimestamp(),
      From_User: userName === "WeChatyBot" ? "Bot" : "User",
    });
  } catch (error) {
    console.error(error);
  }
}

/**
 * Post the review flag for the current student.
 * @param {String} studentId The feedback provided user student id.
 * @param {String} criteria  The criteria selected for setting the flag.
 */
async function postReviewFlag(studentId, criteria) {
  try {
    if (criteria === "Audience Communication")
      await userProfileRef.doc(studentId).update({ Audience_Flag: false });
    else if (criteria === "Visual Interest")
      await userProfileRef.doc(studentId).update({ Visual_Flag: false });
    else await userProfileRef.doc(studentId).update({ Knowledge_Flag: false });
  } catch (error) {
    console.error(error);
  }
}

/**
 * Post the detailed comments for the rank providers.
 * @param {String} studentId The feedback provided user student id.
 * @param {Array} rankProvider List for all the rank providers.
 * @param {String} criteria  The criteria selected for setting the flag.
 * @param {String} ranking Ranking provided by the reviewer.
 * @param {Array} rankingList Ranking list for the current video.
 */
async function postComment(
  studentId,
  rankProvider,
  criteria,
  ranking,
  rankingList
) {
  try {
    const role = await getUserRole(rankProvider);
    let providerRef;
    if (role === "Student")
      providerRef = await userProfileRef.doc(rankProvider);
    else providerRef = await tutorRef.doc(rankProvider);

    await providerRef.collection("Rank_Detail").add({
      Student_Id: studentId,
      Criteria: criteria,
      Rank_Provided: ranking,
      Ranking_List: rankingList,
      Created_At: admin.firestore.FieldValue.serverTimestamp(),
    });
  } catch (error) {
    console.error(error);
  }
}

/**
 * Reset the user list for more rankings from tutors.
 */
async function postUserList() {
  try {
    const snapshot = await userRef.get();
    const backupList = snapshot.data().UserListBackup;
    await userRef.update({ UserList: backupList });
  } catch (error) {
    console.error(error);
  }
}

/* ############################## Firestore UPDATE Methods ############################## */

/**
 * Update the assess status for the 3 documents generated.
 * @param {Array} docs Random documents generated for reallocation.
 */
async function updateAssessStatus(docs) {
  try {
    var rankReceivedNo_1 = docs[0].data().Rank_Received_No;
    var rankReceivedNo_2 = docs[1].data().Rank_Received_No;
    var rankReceivedNo_3 = docs[2].data().Rank_Received_No;

    // Update the received number if not exceeding the maximum feedback number.
    if (rankReceivedNo_1 < MAX_FEEDBACK_NO * 3 - 1)
      assessStatusRef
        .doc(docs[0].id)
        .update({ Rank_Received_No: ++rankReceivedNo_1 });
    // Otherwise, remove the reference node.
    else assessStatusRef.doc(docs[0].id).delete();

    if (rankReceivedNo_2 < MAX_FEEDBACK_NO * 3 - 1)
      assessStatusRef
        .doc(docs[1].id)
        .update({ Rank_Received_No: ++rankReceivedNo_2 });
    // Otherwise, remove the reference node.
    else assessStatusRef.doc(docs[1].id).delete();

    if (rankReceivedNo_3 < MAX_FEEDBACK_NO * 3 - 1)
      assessStatusRef
        .doc(docs[2].id)
        .update({ Rank_Received_No: ++rankReceivedNo_3 });
    // Otherwise, remove the reference node.
    else assessStatusRef.doc(docs[2].id).delete();
    return true;
  } catch (error) {
    console.error(error);
    return false;
  }
}

/**
 * Update the reply flag (for video processing indication).
 * @param {String} id Current user's monash id number.
 * @param {String} role Current user's role.
 * @param {Boolena} flag Flag to be set for the reply flag.
 */
async function updateReplyFlag(id, role, flag) {
  try {
    if (role === "Student")
      await userProfileRef.doc(id).update({ Reply_Flag: flag });
    else await tutorRef.doc(id).update({ Reply_Flag: flag });
  } catch (error) {
    console.error(error);
  }
}

/**
 * Update the ranking details with the detailed message and timestamp.
 * @param {String} documentId The ranking detail document id.
 * @param {String} rankProvider Rank provider's student id.
 * @param {String} role Role of the rank provider.
 * @param {String} message Detailed comment for the ranking.
 */
async function updateRankDetail(documentId, rankProvider, role, message) {
  try {
    let providerRef;
    if (role === "Student") providerRef = userProfileRef.doc(rankProvider);
    else providerRef = tutorRef.doc(rankProvider);

    await providerRef
      .collection("Rank_Detail")
      .doc(documentId)
      .update({
        Detail_Message: message,
        Reply_At: admin.firestore.FieldValue.serverTimestamp(),
      });
  } catch (error) {
    console.error(error);
  }
}

/**
 * Update the user id and last seen timestamp for webchat users.
 * @param {String} id Current user id.
 * @param {String} userId The user id from the current channel.
 * @param {String} role Role of the current user.
 * @param {String} channel The channel used by the user.
 */
async function updateUserProfile(id, role, userId, channel) {
  try {
    let currentRef;
    if (role === "Student") currentRef = userProfileRef.doc(id);
    else currentRef = tutorRef.doc(id);

    await currentRef.update({
      User_Id: userId,
      Channel: channel,
      Last_Seen: admin.firestore.FieldValue.serverTimestamp(),
    });
  } catch (error) {
    console.error(error);
  }
}

/**
 * Update user list for tutor ranking.
 * @param {Array} userList
 */
async function updateUserList(userList) {
  try {
    await userRef.update({ UserList: userList });
  } catch (error) {
    console.error(error);
  }
}

/**
 *
 * @param {String} userId The user id from the current channel.
 */
async function deleteIdleDetection(userId) {
  try {
    await idleRef.doc(userId).delete();
  } catch (error) {
    console.error(error);
  }
}

/* ############################## Firestore Additional Methods ############################## */

/**
 * Initialize firestore with default data for testing purpose.
 */
async function initializeDb() {
  try {
    // Reset the settings.
    await settingRef.set(CONSTANTS.DEFAULT_SETTING);
    await settingRef.update({ Previous_Setting: CONSTANTS.DEFAULT_SETTING });
    await wechatSettingRef.set(CONSTANTS.DEFAULT_SETTING);
    await wechatSettingRef.update({
      Previous_Setting: CONSTANTS.DEFAULT_SETTING,
    });

    // Create default user data.
    await userRef.set({ UserCount: 0 });
    for (var i = 0; i < 10; i++) {
      await userProfileRef.doc(`3000000${i}`).set({
        Rank_Provided_No: 0,
        Rank_Received_No: 0,
        Reply_Flag: true,
        Video_Url: "https://1drv.ms/v/s!AgWtDcpur3eUhEwRIPfQVZWaM7h9",
        Submission_Time: admin.firestore.FieldValue.serverTimestamp(),
      });
    }
  } catch (error) {
    console.error(error);
  }
}

/**
 * Add header for the console.log messages (script name + timestamp).
 */
function echoHead() {
  const scriptName = path.basename(__filename);
  const currentTime = new Date().toLocaleString("en-US");
  return scriptName + " " + currentTime;
}

/**
 * Shuffle the array randomly.
 * @param {Array} array The array to be shuffled
 */
function shuffle(array) {
  var i = array.length,
    j = 0,
    temp;
  while (i--) {
    j = Math.floor(Math.random() * (i + 1));
    // Swap randomly chosen element with current element.
    temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
  return array;
}

/* Export Firestore GET Methods */
exports.getMonashId = getMonashId;
exports.getUserProfile = getUserProfile;
exports.getAllUserId = getAllUserId;
exports.getConversationRef = getConversationRef;
exports.getAllConversations = getAllConversations;
exports.getStudentVideo = getStudentVideo;
exports.getCurrentStage = getCurrentStage;
exports.getRankDetails = getRankDetails;
exports.getRandomVideos = getRandomVideos;
exports.getUserCount = getUserCount;
exports.getUserList = getUserList;
exports.getStudentIdByIndex = getStudentIdByIndex;
exports.getVideoUrl = getVideoUrl;
exports.getThumbnailUrl = getThumbnailUrl;
exports.getRankReceived = getRankReceived;

/* Export Firestore POST Methods */
exports.postStudentIndex = postStudentIndex;
exports.postAssessStatus = postAssessStatus;
exports.postUser = postUser;
exports.postWebChatUser = postWebChatUser;
exports.postIdleDetection = postIdleDetection;
exports.postVideoSubmission = postVideoSubmission;
exports.postRanking = postRanking;
exports.postConversation = postConversation;
exports.postFile = postFile;
exports.postLogMessage = postLogMessage;
exports.postReviewFlag = postReviewFlag;
exports.postComment = postComment;
exports.postUserList = postUserList;

/* Export Firestore UPDATE Methods */
exports.updateAssessStatus = updateAssessStatus;
exports.updateReplyFlag = updateReplyFlag;
exports.updateRankDetail = updateRankDetail;
exports.updateUserList = updateUserList;
exports.deleteIdleDetection = deleteIdleDetection;

/* Export Firestore Additional Helper Methods */
exports.initializeDb = initializeDb;
