const { ActivityHandler, TurnContext } = require("botbuilder");
const { postConversation } = require("../firebase/firebaseHelper");
const CONSTANTS = require("../constants");

class DialogBot extends ActivityHandler {
  /**
   * @param {ConversationState} conversationState
   * @param {UserState} userState
   * @param {Dialog} dialog
   */
  constructor(conversationState, userState, dialog, conversationReferences) {
    super();

    // Dependency injected dictionary for storing ConversationReference objects used in
    // NotifyController to proactively message users.
    this.conversationReferences = conversationReferences;

    this.onConversationUpdate(async (context, next) => {
      this.addConversationReference(context.activity);

      await next();
    });

    if (!conversationState)
      throw new Error(
        "[DialogBot]: Missing parameter. conversationState is required"
      );
    if (!userState)
      throw new Error("[DialogBot]: Missing parameter. userState is required");
    if (!dialog)
      throw new Error("[DialogBot]: Missing parameter. dialog is required");

    this.conversationState = conversationState;
    this.userState = userState;
    this.dialog = dialog;
    this.dialogState = this.conversationState.createProperty("DialogState");

    this.onMembersAdded(async (context, next) => {
      const membersAdded = context.activity.membersAdded;
      const channel = context.activity.channelId.toUpperCase();

      for (let cnt = 0; cnt < membersAdded.length; cnt++) {
        const idEqual = membersAdded[cnt].id === context.activity.recipient.id;
        // Welcoming users for WebChat and Other Channels are different. For more information can be found at:
        // https://stackoverflow.com/questions/56111293/botframework-how-to-fixwelcome-message-is-not-getting-displayed-to-the-user
        if (channel === "WEBCHAT") {
          if (idEqual)
            await context.sendActivity(
              CONSTANTS.BROADCAST_MESSAGE.WelcomeWebChat
            );
        } else {
          if (!idEqual) {
            if (channel !== "EMULATOR")
              await context.sendActivity(CONSTANTS.BROADCAST_MESSAGE.Welcome);
            else
              await context.sendActivity(
                CONSTANTS.BROADCAST_MESSAGE.WelcomeWebChat
              );
          }
        }
      }

      // By calling next() you ensure that the next BotHandler is run.
      await next();
    });

    this.onMessage(async (context, next) => {
      this.addConversationReference(context.activity);

      // Run the Dialog with the new message Activity.
      await this.dialog.run(context, this.dialogState);

      // By calling next() you ensure that the next BotHandler is run.
      await next();
    });

    this.onDialog(async (context, next) => {
      // Save any state changes. The load happened during the execution of the Dialog.
      await this.conversationState.saveChanges(context, false);
      await this.userState.saveChanges(context, false);

      // By calling next() you ensure that the next BotHandler is run.
      await next();
    });
  }

  async addConversationReference(activity) {
    const conversationReference = TurnContext.getConversationReference(
      activity
    );

    this.conversationReferences[
      conversationReference.conversation.id
    ] = conversationReference;

    // Update the conversation details in Firebase.
    const userId = activity.from.id;
    const userName = activity.from.name;
    const channel = activity.channelId.toUpperCase();

    if (userName !== undefined)
      await postConversation(
        channel,
        userId,
        conversationReference.conversation.id,
        JSON.stringify(conversationReference)
      );
  }
}

module.exports.DialogBot = DialogBot;
