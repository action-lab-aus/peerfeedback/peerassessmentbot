// Define the constants which will be reused.
module.exports = {
  BROADCAST_MESSAGE: {
    Welcome:
      "Ahoy! I'm the peer feedback bot. I’ll be sending you some simple tasks to complete over the next few days, but firstly... what should I call you?",
    WelcomeWebChat:
      "Ahoy! I'm the peer feedback bot. Please be aware that using WebChat will require you to authenticate with your Monash ID every time and certain user experiences might also be compromised. Please use WeChat if you can to achieve the ultimate experience. I’ll be sending you some simple tasks to complete over the next few days, but firstly... what should I call you?",
    Upload:
      "Please submit a link to your draft video. 1. We can only process public Google Drive, Spark or OneDrive Personal links. 2. Videos must be less than 100MB. I will take some time to verify the video link, so if I don't reply immediately, don't panic!",
    Ranking:
      "Everyone has now submitted a video. Next, I will ask you to rank (order) different groups of videos. In each group, I will show you three videos (A,B,C) which you need to rank in order for a few criteria. /:strong when you want the first video.",
    Reallocate:
      "We still have videos which require feedback. Send /:strong if you are up for doing more ranking!",
    Review:
      "Rankings for your video are ready. /:strong when you want to see them.",
    Summary:
      "A recipient of your ranking would like a little more information about how you made your decision. /:strong when you have some time to go into more detail.",
  },
  DEFAULT_SETTING: {
    Registration: true,
    Upload: false,
    Ranking: false,
    Reallocate: false,
    Review: false,
    Summary: false,
  },
};
