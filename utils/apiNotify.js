const axios = require("axios");
const baseDirectlinUrl =
  "https://wechat-bot-connector.azurewebsites.net/notify";
const baseChannelUrl = "https://peerfeedback-8680.azurewebsites.net/api/notify";
// const baseChannelUrl = "https://c7ee5342c1ae.ngrok.io" + "/api/notify";
// const baseChannelUrl = "http://localhost:3980/api/notify";

/**
 * Send activity to the bot framework /api/notify endpoint for notifying the video submission.
 * @param {String} channel The channel used by the student.
 * @param {String} userId The useId from the current channel.
 * @param {String} message The message to be sent to the user.
 * @param {String} videoName The uploaded video name, undefined if not uploaded successfully.
 * @param {Boolean} flag Flag to indicate whether the message is about start video transcoding.
 */
async function sendVideoConfirmation(
  channel,
  userId,
  message,
  videoName,
  flag
) {
  const url = channel === "DIRECTLINE" ? baseDirectlinUrl : baseChannelUrl;
  try {
    return await axios({
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      url: url,
      data: {
        type: "message",
        channel: channel,
        userId: userId,
        message: message,
        videoName: videoName,
        flag: flag,
      },
    });
  } catch (error) {
    console.error(error);
    return error.response;
  }
}

/**
 * Send activity to the bot framework /api/notify/review endpoint for notifying the detailed ranking comment.
 * @param {String} channel The channel used by the student.
 * @param {String} userId The useId from the current channel.
 * @param {Object} conversationRef Conversation reference for the user.
 * @param {String} message The message to be sent to the user.
 */
async function sendDetailedReview(channel, userId, conversationRef, message) {
  const url =
    (channel === "DIRECTLINE" ? baseDirectlinUrl : baseChannelUrl) + "/review";
  try {
    return await axios({
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      url: url,
      data: {
        type: "message",
        message: message,
        userId: userId,
        channel: channel,
        conversationRef: conversationRef,
      },
    });
  } catch (error) {
    console.error(error);
    return error.response;
  }
}

exports.sendVideoConfirmation = sendVideoConfirmation;
exports.sendDetailedReview = sendDetailedReview;
