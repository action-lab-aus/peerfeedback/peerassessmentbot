const { postLogMessage } = require("../firebase/firebaseHelper");

class CustomLogger {
  /**
   * Log an activity to the transcript.
   * @param activity Activity being logged.
   */
  async logActivity(activity) {
    if (!activity) {
      throw new Error("Activity is required.");
    }

    if (activity.conversation) {
      const logText = JSON.stringify(activity);

      // Get the user id from the current activity
      var userId = activity.from.id;

      // Get the user name, if the sender is bot, set the user id to be the recipient
      var userName = activity.from.name;
      if (userName === "PeerFeedback") userId = activity.recipient.id;

      // Get the timestamp and channel
      const timestamp = activity.timestamp;
      const channel = activity.channelId.toUpperCase();

      if (userName !== undefined && userId !== "BotActivator")
        postLogMessage(channel, userId, timestamp, logText, userName);
    }
  }
}
exports.CustomLogger = CustomLogger;
