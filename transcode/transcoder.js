const AWS = require("aws-sdk");
AWS.config.update({ region: process.env.AWS_REGION });
AWS.config.mediaconvert = { endpoint: process.env.AWS_MEDIACONVERT_ENDPOINT };
const S3 = new AWS.S3();
process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
const BUCKET_NAME = "peer-feedback";

const fs = require("fs");
const axios = require("axios");
const cheerio = require("cheerio");
const {
  updateReplyFlag,
  postFile,
  getUserProfile,
} = require("../firebase/firebaseHelper");
const { sendVideoConfirmation } = require("../utils/apiNotify");
const { getMediaConvertParam } = require("./mediaConvertParam");

/**
 * Start the downloading process based on the Url provided.
 * Check the credential and authorization status first before downloading the file.
 * @param {String} fileUrl Google Drive/Spark/OneDrive Personal video Url.
 * @param {String} id The user id for the video.
 * @param {String} channel The channel used by the user.
 * @param {String} userId The useId from the current channel.
 */
async function downloadVideo(fileUrl, id, channel, userId) {
  // Extract the file id from Google Drive link and reconstruct the url.
  const url = await getDownloadUrl(fileUrl);
  const role = "Student";
  if (!url) {
    await updateReplyFlag(id, role, true);
    const message =
      "I couldn't get to the Spark link you sent 😵 Could you check it and resend? A valid Spark video link will look like `https://spark.adobe.com/video/xxxxxxxxxxxxx`.";
    return await sendVideoConfirmation(channel, userId, message);
  }

  // For Spark video, send the downloadable url directly to AWS media convert for transcoding.
  if (fileUrl.startsWith("https://spark.adobe.com/video/")) {
    try {
      await axios({
        method: "head",
        url: url,
      }).then(async function(response) {
        const contentType = response.headers["content-type"];
        console.log("Content Type: ", contentType);

        // If the content is not video, then return error indicating invalid file type.
        if (contentType.indexOf("video") < 0) {
          await updateReplyFlag(id, role, true);
          const message =
            "I couldn't get to the Spark link you sent 😵 Could you check it and resend? A valid Spark video link will look like `https://spark.adobe.com/video/xxxxxxxxxxxxx`.";
          return await sendVideoConfirmation(channel, userId, message);
        }

        // Otherwise, check the file size through content length.
        // If it is larger than 150MB, ask users to compress & resubmit the video.
        const contentLength =
          parseInt(response.headers["content-length"]) / 1024 / 1024;
        console.log("Content Length: ", contentLength);
        if (contentLength >= 100) {
          await updateReplyFlag(id, role, true);
          const message =
            "The video that you sent is larger than 100 MB 😵 Please reduce the length of your video in Spark and resend the link.";
          return await sendVideoConfirmation(channel, userId, message);
        }

        // Create a Media Convert job on AWS.
        let message =
          "Thanks! It will take a couple of minutes to grab and process your video.";
        if (channel === "WEBCHAT")
          message +=
            " Please keep the chat window open until I let you know that I have processed your video in case you need to resend it.";
        await sendVideoConfirmation(channel, userId, message, null, true);
        createMediaConvertJob(url, id);
      });
    } catch (err) {
      // console.error("Error: ", err);
      await updateReplyFlag(id, role, true);
      const message =
        "The link that you provided is invalid 😵 Could you send me the link to your video again?";
      return await sendVideoConfirmation(channel, userId, message);
    }
  }
  // Otherwise, download and upload video to S3 first before submitting the AWS media convert.
  else {
    try {
      await axios({
        method: "get",
        url: url,
        responseType: "stream",
      }).then(async function(response) {
        const contentType = response.headers["content-type"];
        console.log("Content Type: ", contentType);

        // If the content is not video, then return error indicating invalid file type.
        if (contentType.indexOf("video") < 0) {
          await updateReplyFlag(id, role, true);
          const message =
            "I couldn't download your video from the link you provided 😵 Check the following and try again: 1. Link is public to anyone with the link 2. Size is less than 100 MB 3. Link points to a video file.";
          return await sendVideoConfirmation(channel, userId, message);
        }

        const localFileName = __dirname + `/${id}.mp4`;
        const fileKey = `${id}.mp4`;

        response.data
          .pipe(fs.createWriteStream(localFileName))
          .on("finish", function() {
            fs.readFile(localFileName, (err, data) => {
              if (err) throw err;

              const params = {
                Bucket: BUCKET_NAME,
                Key: fileKey,
                Body: data,
              };

              S3.putObject(params, async function(err, data) {
                if (err) console.log("Error: ", err);
                else {
                  s3Url = `s3://${BUCKET_NAME}/${fileKey}`;
                  fs.unlinkSync(localFileName);
                  let message =
                    "Thanks! It will take a couple of minutes to grab and process your video.";
                  if (channel === "WEBCHAT")
                    message +=
                      " Please keep the chat window open until I let you know that I have processed your video in case you need to resend it.";
                  await sendVideoConfirmation(
                    channel,
                    userId,
                    message,
                    null,
                    true
                  );
                  // Create a Media Convert job on AWS.
                  createMediaConvertJob(s3Url, id);
                }
              });
            });
          });
      });
    } catch (err) {
      // console.error(err);
      await updateReplyFlag(id, role, true);
      const message =
        "The link that you provided is invalid 😵 Could you send me the link to your video again?";
      return await sendVideoConfirmation(channel, userId, message);
    }
  }
}

/**
 * Get downloadable url from the shared url
 * @param {String} fileUrl Google Drive/Spark/OneDrive Personal video Url
 */
async function getDownloadUrl(fileUrl) {
  let url;

  // Parse Google Drive Url
  if (fileUrl.startsWith("https://drive.google.com/")) {
    const id = fileUrl.match(/[-\w]{25,}/);
    url = `https://drive.google.com/uc?export=download&id=${id}`;
  }

  // Parse OneDrive Personal Url
  if (fileUrl.startsWith("https://1drv.ms/")) {
    const base64Value = Buffer.from(fileUrl).toString("base64");
    url = `https://api.onedrive.com/v1.0/shares/u!${base64Value}/root/content`;
  }

  // Parse Adobe Spark Url
  if (fileUrl.startsWith("https://spark.adobe.com/video/")) {
    const videoUrl = fileUrl + "/embed";
    try {
      const response = await axios({ method: "get", url: videoUrl });
      // Use Cheerio to extract the video src url.
      const $ = cheerio.load(response.data);
      $("video source").each((i, elem) => {
        if ($(elem).attr("type") === "video/mp4") {
          url = $(elem).attr("src");
          return false;
        }
      });
    } catch (err) {
      console.error("Error: ", err);
      url = null;
    }
  }
  return url;
}

/**
 * Create a media convert job on AWS with defined parameters.
 * @param {String} url S3 or Adobe Spark video url for transcoding.
 * @param {String} id The user id for the video.
 */
function createMediaConvertJob(url, id) {
  const params = getMediaConvertParam(url, `-${id}`);

  // Create a promise on a MediaConvert object.
  var endpointPromise = new AWS.MediaConvert({ apiVersion: "2017-08-29" })
    .createJob(params)
    .promise();

  // Handle promise's fulfilled/rejected status.
  endpointPromise.then(
    function(data) {
      // console.log("Meida Convert Job Created: ", data);
    },
    function(err) {
      console.error(err);
    }
  );
}

/**
 * Download S3 resources created by the media convert job and ask user for video confirmation.
 * @param {*} outputGroup S3 resource output group for the meida convert job
 */
async function downloadS3Resource(outputGroup) {
  const role = "Student";
  try {
    for (let i = 0; i < outputGroup.length; i++) {
      const s3FileKey = outputGroup[i].outputFilePaths[0].split("/")[3];

      let temp = s3FileKey.split("-");
      const id = temp[temp.length - 1].split(".")[0];

      temp = s3FileKey.split(".");
      const fileType = temp[temp.length - 1];

      const fileName = id + "." + fileType;
      const filePath = __dirname + "/" + id + "." + fileType;

      const options = {
        Bucket: BUCKET_NAME,
        Key: s3FileKey,
      };

      var fileStream = fs.createWriteStream(filePath);
      var s3Stream = S3.getObject(options).createReadStream();

      // Listen for errors returned by the service.
      s3Stream.on("error", function(err) {
        // NoSuchKey: The specified key does not exist.
        console.error(err);
      });

      s3Stream
        .pipe(fileStream)
        .on("error", function(err) {
          // Capture any errors that occur when writing data to the file.
          console.error("File Stream Error:", err);
        })
        .on("close", async function() {
          // Get file details including metadata and post file to Firebase.
          const prefix = fileType === "mp4" ? "Videos" : "Thumbnails";
          const contentType = fileType === "mp4" ? "video/mp4" : "image/jpeg";
          const metadata = { contentType: contentType };

          await postFile(filePath, prefix, metadata, fileName);
          // Remove file from local disk.
          fs.unlinkSync(filePath);

          if (fileType === "mp4") {
            // Send notifications to the user saying video transcoding successful.
            const message =
              "Is this image from the video you wanted to send me? Confirm with yes/no.";

            const { channel, userId } = await getUserProfile(id, role);
            await sendVideoConfirmation(channel, userId, message, fileName);
          }
        });
    }
  } catch (error) {
    console.error(error);
  }
}

exports.downloadVideo = downloadVideo;
exports.downloadS3Resource = downloadS3Resource;
